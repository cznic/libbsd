// Code generated for linux/loong64 by 'gcc -no-main-minimize --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-link-errors -ignore-unsupported-alignment -ignore-link-errors -I /home/cznic/src/modernc.org/builder/.exclude/modernc.org/libc/include/linux/loong64 -I /home/cznic/src/modernc.org/builder/.exclude/modernc.org/limd/include/linux/loong64 -lbsd -DNDEBUG -o .libs/md5.go md5.o.go -lmd -lbsd', DO NOT EDIT.

//go:build linux && loong64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_BIG_ENDIAN = "__BIG_ENDIAN"
const m_BYTE_ORDER = "__BYTE_ORDER"
const m_FD_SETSIZE = 1024
const m_HAVE_ASPRINTF = 1
const m_HAVE_CLEARENV = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DECL_ENVIRON = 1
const m_HAVE_DIRENT_H = 1
const m_HAVE_DIRFD = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_FLOCK = 1
const m_HAVE_FOPENCOOKIE = 1
const m_HAVE_GETAUXVAL = 1
const m_HAVE_GETENTROPY = 1
const m_HAVE_GETLINE = 1
const m_HAVE_GRP_H = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_OPEN_MEMSTREAM = 1
const m_HAVE_PROGRAM_INVOCATION_SHORT_NAME = 1
const m_HAVE_PWD_H = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_EXT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYSCONF = 1
const m_HAVE_SYS_DIR_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_TYPEOF = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_VASPRINTF = 1
const m_HAVE_WCHAR_H = 1
const m_HAVE___FPURGE = 1
const m_HAVE___PROGNAME = 1
const m_INT16_MAX = 0x7fff
const m_INT32_MAX = 0x7fffffff
const m_INT64_MAX = 0x7fffffffffffffff
const m_INT8_MAX = 0x7f
const m_INTMAX_MAX = "INT64_MAX"
const m_INTMAX_MIN = "INT64_MIN"
const m_INTPTR_MAX = "INT64_MAX"
const m_INTPTR_MIN = "INT64_MIN"
const m_INT_FAST16_MAX = "INT32_MAX"
const m_INT_FAST16_MIN = "INT32_MIN"
const m_INT_FAST32_MAX = "INT32_MAX"
const m_INT_FAST32_MIN = "INT32_MIN"
const m_INT_FAST64_MAX = "INT64_MAX"
const m_INT_FAST64_MIN = "INT64_MIN"
const m_INT_FAST8_MAX = "INT8_MAX"
const m_INT_FAST8_MIN = "INT8_MIN"
const m_INT_LEAST16_MAX = "INT16_MAX"
const m_INT_LEAST16_MIN = "INT16_MIN"
const m_INT_LEAST32_MAX = "INT32_MAX"
const m_INT_LEAST32_MIN = "INT32_MIN"
const m_INT_LEAST64_MAX = "INT64_MAX"
const m_INT_LEAST64_MIN = "INT64_MIN"
const m_INT_LEAST8_MAX = "INT8_MAX"
const m_INT_LEAST8_MIN = "INT8_MIN"
const m_LIBBSD_ABI_ACCMODE = 1
const m_LIBBSD_ABI_ARC4RANDOM = 1
const m_LIBBSD_ABI_ARC4RANDOM_STIR = 1
const m_LIBBSD_ABI_ASPRINTF = 0
const m_LIBBSD_ABI_BSD_GETOPT = 1
const m_LIBBSD_ABI_CLOSEFROM = 1
const m_LIBBSD_ABI_ERR = 0
const m_LIBBSD_ABI_ERRC = 1
const m_LIBBSD_ABI_EXPAND_NUMBER = 1
const m_LIBBSD_ABI_EXPLICIT_BZERO = 1
const m_LIBBSD_ABI_FGETLN = 1
const m_LIBBSD_ABI_FLOPEN = 1
const m_LIBBSD_ABI_FMTCHECK = 1
const m_LIBBSD_ABI_FPURGE = 1
const m_LIBBSD_ABI_FREEZERO = 1
const m_LIBBSD_ABI_FUNOPEN = 1
const m_LIBBSD_ABI_GETBSIZE = 1
const m_LIBBSD_ABI_GETPEEREID = 1
const m_LIBBSD_ABI_HUMANIZE_NUMBER = 1
const m_LIBBSD_ABI_ID_FROM_NAME = 1
const m_LIBBSD_ABI_INET_NET_PTON = 1
const m_LIBBSD_ABI_MD5 = 1
const m_LIBBSD_ABI_NAME_FROM_ID = 1
const m_LIBBSD_ABI_NLIST = 1
const m_LIBBSD_ABI_PIDFILE = 1
const m_LIBBSD_ABI_PROCTITLE = 1
const m_LIBBSD_ABI_PROGNAME = 1
const m_LIBBSD_ABI_PWCACHE = 1
const m_LIBBSD_ABI_READPASSPHRASE = 1
const m_LIBBSD_ABI_REALLOCARRAY = 1
const m_LIBBSD_ABI_REALLOCF = 1
const m_LIBBSD_ABI_RECALLOCARRAY = 1
const m_LIBBSD_ABI_SORT = 1
const m_LIBBSD_ABI_STRINGLIST = 1
const m_LIBBSD_ABI_STRL = 1
const m_LIBBSD_ABI_STRMODE = 1
const m_LIBBSD_ABI_STRNSTR = 1
const m_LIBBSD_ABI_STRTONUM = 1
const m_LIBBSD_ABI_STRTOX = 1
const m_LIBBSD_ABI_TIME64 = 0
const m_LIBBSD_ABI_TIMECONV = 1
const m_LIBBSD_ABI_TRANSPARENT_LIBMD = 1
const m_LIBBSD_ABI_VIS = 1
const m_LIBBSD_ABI_WCSL = 1
const m_LIBBSD_DISABLE_DEPRECATED = 1
const m_LIBBSD_OVERLAY = 1
const m_LIBBSD_SYS_HAS_TIME64 = 1
const m_LIBBSD_SYS_TIME_BITS = 64
const m_LITTLE_ENDIAN = "__LITTLE_ENDIAN"
const m_LT_OBJDIR = ".libs/"
const m_MD5_BLOCK_LENGTH = 64
const m_MD5_DIGEST_LENGTH = 16
const m_NDEBUG = 1
const m_PACKAGE = "libbsd"
const m_PACKAGE_BUGREPORT = "libbsd@lists.freedesktop.org"
const m_PACKAGE_NAME = "libbsd"
const m_PACKAGE_STRING = "libbsd 0.12.0"
const m_PACKAGE_TARNAME = "libbsd"
const m_PACKAGE_URL = ""
const m_PACKAGE_VERSION = "0.12.0"
const m_PDP_ENDIAN = "__PDP_ENDIAN"
const m_PTRDIFF_MAX = "INT64_MAX"
const m_PTRDIFF_MIN = "INT64_MIN"
const m_SIG_ATOMIC_MAX = "INT32_MAX"
const m_SIG_ATOMIC_MIN = "INT32_MIN"
const m_SIZEOF_TIME_T = 8
const m_SIZE_MAX = "UINT64_MAX"
const m_STDC_HEADERS = 1
const m_UINT16_MAX = 0xffff
const m_UINT32_MAX = "0xffffffffu"
const m_UINT64_MAX = "0xffffffffffffffffu"
const m_UINT8_MAX = 0xff
const m_UINTMAX_MAX = "UINT64_MAX"
const m_UINTPTR_MAX = "UINT64_MAX"
const m_UINT_FAST16_MAX = "UINT32_MAX"
const m_UINT_FAST32_MAX = "UINT32_MAX"
const m_UINT_FAST64_MAX = "UINT64_MAX"
const m_UINT_FAST8_MAX = "UINT8_MAX"
const m_UINT_LEAST16_MAX = "UINT16_MAX"
const m_UINT_LEAST32_MAX = "UINT32_MAX"
const m_UINT_LEAST64_MAX = "UINT64_MAX"
const m_UINT_LEAST8_MAX = "UINT8_MAX"
const m_VERSION = "0.12.0"
const m_WINT_MAX = "UINT32_MAX"
const m_WINT_MIN = 0
const m__ABILP64 = 3
const m__ALL_SOURCE = 1
const m__DARWIN_C_SOURCE = 1
const m__GNU_SOURCE = 1
const m__HPUX_ALT_XOPEN_SOCKET_API = 1
const m__LOONGARCH_ARCH = "la64v1.0"
const m__LOONGARCH_FPSET = 32
const m__LOONGARCH_SIM = "_ABILP64"
const m__LOONGARCH_SPFPSET = 32
const m__LOONGARCH_SZINT = 32
const m__LOONGARCH_SZLONG = 64
const m__LOONGARCH_SZPTR = 64
const m__LOONGARCH_TUNE = "generic"
const m__LP64 = 1
const m__NETBSD_SOURCE = 1
const m__OPENBSD_SOURCE = 1
const m__POSIX_PTHREAD_SEMANTICS = 1
const m__STDC_PREDEF_H = 1
const m__SYS_CDEFS_H = 1
const m__TANDEM_SOURCE = 1
const m___ACCUM_EPSILON__ = "0x1P-15K"
const m___ACCUM_FBIT__ = 15
const m___ACCUM_IBIT__ = 16
const m___ACCUM_MAX__ = "0X7FFFFFFFP-15K"
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BIG_ENDIAN = 4321
const m___BYTE_ORDER = 1234
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___DA_FBIT__ = 31
const m___DA_IBIT__ = 32
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DIG__ = 15
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_IS_IEC_60559__ = 1
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DECIMAL_DIG__ = 36
const m___DEC_EVAL_METHOD__ = 2
const m___DQ_FBIT__ = 63
const m___DQ_IBIT__ = 0
const m___ELF__ = 1
const m___EXTENSIONS__ = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLOAT128_TYPE__ = 1
const m___FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___FLT128_DECIMAL_DIG__ = 36
const m___FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT128_DIG__ = 33
const m___FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT128_HAS_DENORM__ = 1
const m___FLT128_HAS_INFINITY__ = 1
const m___FLT128_HAS_QUIET_NAN__ = 1
const m___FLT128_IS_IEC_60559__ = 1
const m___FLT128_MANT_DIG__ = 113
const m___FLT128_MAX_10_EXP__ = 4932
const m___FLT128_MAX_EXP__ = 16384
const m___FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT32X_DECIMAL_DIG__ = 17
const m___FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT32X_DIG__ = 15
const m___FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT32X_HAS_DENORM__ = 1
const m___FLT32X_HAS_INFINITY__ = 1
const m___FLT32X_HAS_QUIET_NAN__ = 1
const m___FLT32X_IS_IEC_60559__ = 1
const m___FLT32X_MANT_DIG__ = 53
const m___FLT32X_MAX_10_EXP__ = 308
const m___FLT32X_MAX_EXP__ = 1024
const m___FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32_DECIMAL_DIG__ = 9
const m___FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT32_DIG__ = 6
const m___FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT32_HAS_DENORM__ = 1
const m___FLT32_HAS_INFINITY__ = 1
const m___FLT32_HAS_QUIET_NAN__ = 1
const m___FLT32_IS_IEC_60559__ = 1
const m___FLT32_MANT_DIG__ = 24
const m___FLT32_MAX_10_EXP__ = 38
const m___FLT32_MAX_EXP__ = 128
const m___FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT64X_DECIMAL_DIG__ = 36
const m___FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT64X_DIG__ = 33
const m___FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT64X_HAS_DENORM__ = 1
const m___FLT64X_HAS_INFINITY__ = 1
const m___FLT64X_HAS_QUIET_NAN__ = 1
const m___FLT64X_IS_IEC_60559__ = 1
const m___FLT64X_MANT_DIG__ = 113
const m___FLT64X_MAX_10_EXP__ = 4932
const m___FLT64X_MAX_EXP__ = 16384
const m___FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64_DECIMAL_DIG__ = 17
const m___FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT64_DIG__ = 15
const m___FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT64_HAS_DENORM__ = 1
const m___FLT64_HAS_INFINITY__ = 1
const m___FLT64_HAS_QUIET_NAN__ = 1
const m___FLT64_IS_IEC_60559__ = 1
const m___FLT64_MANT_DIG__ = 53
const m___FLT64_MAX_10_EXP__ = 308
const m___FLT64_MAX_EXP__ = 1024
const m___FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT_EVAL_METHOD_TS_18661_3__ = 0
const m___FLT_EVAL_METHOD__ = 0
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_IS_IEC_60559__ = 1
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_RADIX__ = 2
const m___FP_FAST_FMA = 1
const m___FP_FAST_FMAF = 1
const m___FP_FAST_FMAF32 = 1
const m___FP_FAST_FMAF32x = 1
const m___FP_FAST_FMAF64 = 1
const m___FRACT_EPSILON__ = "0x1P-15R"
const m___FRACT_FBIT__ = 15
const m___FRACT_IBIT__ = 0
const m___FRACT_MAX__ = "0X7FFFP-15R"
const m___FUNCTION__ = "__func__"
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GCC_IEC_559 = 2
const m___GCC_IEC_559_COMPLEX = 2
const m___GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const m___GNUC_MINOR__ = 1
const m___GNUC_PATCHLEVEL__ = 0
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const m___GNUC__ = 14
const m___GXX_ABI_VERSION = 1019
const m___HAVE_GENERIC_SELECTION = 0
const m___HAVE_SPECULATION_SAFE_VALUE = 1
const m___HA_FBIT__ = 7
const m___HA_IBIT__ = 8
const m___HQ_FBIT__ = 15
const m___HQ_IBIT__ = 0
const m___INT16_MAX__ = 0x7fff
const m___INT32_MAX__ = 0x7fffffff
const m___INT32_TYPE__ = "int"
const m___INT64_MAX__ = 0x7fffffffffffffff
const m___INT8_MAX__ = 0x7f
const m___INTMAX_MAX__ = 0x7fffffffffffffff
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_MAX__ = 0x7fffffffffffffff
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_MAX__ = 0x7fffffffffffffff
const m___INT_FAST16_WIDTH__ = 64
const m___INT_FAST32_MAX__ = 0x7fffffffffffffff
const m___INT_FAST32_WIDTH__ = 64
const m___INT_FAST64_MAX__ = 0x7fffffffffffffff
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_MAX__ = 0x7f
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_MAX__ = 0x7fff
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_MAX__ = 0x7fffffff
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_MAX__ = 0x7fffffffffffffff
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_MAX__ = 0x7f
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 0x7fffffff
const m___INT_WIDTH__ = 32
const m___LACCUM_EPSILON__ = "0x1P-31LK"
const m___LACCUM_FBIT__ = 31
const m___LACCUM_IBIT__ = 32
const m___LACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFP-31LK"
const m___LDBL_DECIMAL_DIG__ = 36
const m___LDBL_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___LDBL_DIG__ = 33
const m___LDBL_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_IS_IEC_60559__ = 1
const m___LDBL_MANT_DIG__ = 113
const m___LDBL_MAX_10_EXP__ = 4932
const m___LDBL_MAX_EXP__ = 16384
const m___LDBL_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___LDBL_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___LDBL_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___LDOUBLE_REDIRECTS_TO_FLOAT128_ABI = 0
const m___LFRACT_EPSILON__ = "0x1P-31LR"
const m___LFRACT_FBIT__ = 31
const m___LFRACT_IBIT__ = 0
const m___LFRACT_MAX__ = "0X7FFFFFFFP-31LR"
const m___LITTLE_ENDIAN = 1234
const m___LLACCUM_EPSILON__ = "0x1P-63LLK"
const m___LLACCUM_FBIT__ = 63
const m___LLACCUM_IBIT__ = 64
const m___LLACCUM_MAX__ = "0X7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFP-63LLK"
const m___LLFRACT_EPSILON__ = "0x1P-63LLR"
const m___LLFRACT_FBIT__ = 63
const m___LLFRACT_IBIT__ = 0
const m___LLFRACT_MAX__ = "0X7FFFFFFFFFFFFFFFP-63LLR"
const m___LONG_LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_LONG_WIDTH__ = 64
const m___LONG_MAX = 0x7fffffffffffffff
const m___LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___NO_INLINE__ = 1
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PDP_ENDIAN = 3412
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTRDIFF_MAX__ = 0x7fffffffffffffff
const m___PTRDIFF_WIDTH__ = 64
const m___QQ_FBIT__ = 7
const m___QQ_IBIT__ = 0
const m___REDIRECT_FORTIFY = "__REDIRECT"
const m___REDIRECT_FORTIFY_NTH = "__REDIRECT_NTH"
const m___REENTRANT = 1
const m___REGISTER_PREFIX__ = "$"
const m___SACCUM_EPSILON__ = "0x1P-7HK"
const m___SACCUM_FBIT__ = 7
const m___SACCUM_IBIT__ = 8
const m___SACCUM_MAX__ = "0X7FFFP-7HK"
const m___SA_FBIT__ = 15
const m___SA_IBIT__ = 16
const m___SCHAR_MAX__ = 0x7f
const m___SCHAR_WIDTH__ = 8
const m___SFRACT_EPSILON__ = "0x1P-7HR"
const m___SFRACT_FBIT__ = 7
const m___SFRACT_IBIT__ = 0
const m___SFRACT_MAX__ = "0X7FP-7HR"
const m___SHRT_MAX__ = 0x7fff
const m___SHRT_WIDTH__ = 16
const m___SIG_ATOMIC_MAX__ = 0x7fffffff
const m___SIG_ATOMIC_TYPE__ = "int"
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_MAX__ = 0xffffffffffffffff
const m___SIZE_WIDTH__ = 64
const m___SQ_FBIT__ = 31
const m___SQ_IBIT__ = 0
const m___STDC_HOSTED__ = 1
const m___STDC_IEC_559_COMPLEX__ = 1
const m___STDC_IEC_559__ = 1
const m___STDC_IEC_60559_BFP__ = 201404
const m___STDC_IEC_60559_COMPLEX__ = 201404
const m___STDC_ISO_10646__ = 201706
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC_WANT_IEC_60559_ATTRIBS_EXT__ = 1
const m___STDC_WANT_IEC_60559_BFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_DFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_FUNCS_EXT__ = 1
const m___STDC_WANT_IEC_60559_TYPES_EXT__ = 1
const m___STDC_WANT_LIB_EXT2__ = 1
const m___STDC_WANT_MATH_SPEC_FUNCS__ = 1
const m___STDC__ = 1
const m___TA_FBIT__ = 63
const m___TA_IBIT__ = 64
const m___TQ_FBIT__ = 127
const m___TQ_IBIT__ = 0
const m___UACCUM_EPSILON__ = "0x1P-16UK"
const m___UACCUM_FBIT__ = 16
const m___UACCUM_IBIT__ = 16
const m___UACCUM_MAX__ = "0XFFFFFFFFP-16UK"
const m___UACCUM_MIN__ = "0.0UK"
const m___UDA_FBIT__ = 32
const m___UDA_IBIT__ = 32
const m___UDQ_FBIT__ = 64
const m___UDQ_IBIT__ = 0
const m___UFRACT_EPSILON__ = "0x1P-16UR"
const m___UFRACT_FBIT__ = 16
const m___UFRACT_IBIT__ = 0
const m___UFRACT_MAX__ = "0XFFFFP-16UR"
const m___UFRACT_MIN__ = "0.0UR"
const m___UHA_FBIT__ = 8
const m___UHA_IBIT__ = 8
const m___UHQ_FBIT__ = 16
const m___UHQ_IBIT__ = 0
const m___UINT16_MAX__ = 0xffff
const m___UINT32_MAX__ = 0xffffffff
const m___UINT64_MAX__ = 0xffffffffffffffff
const m___UINT8_MAX__ = 0xff
const m___UINTMAX_MAX__ = 0xffffffffffffffff
const m___UINTPTR_MAX__ = 0xffffffffffffffff
const m___UINT_FAST16_MAX__ = 0xffffffffffffffff
const m___UINT_FAST32_MAX__ = 0xffffffffffffffff
const m___UINT_FAST64_MAX__ = 0xffffffffffffffff
const m___UINT_FAST8_MAX__ = 0xff
const m___UINT_LEAST16_MAX__ = 0xffff
const m___UINT_LEAST32_MAX__ = 0xffffffff
const m___UINT_LEAST64_MAX__ = 0xffffffffffffffff
const m___UINT_LEAST8_MAX__ = 0xff
const m___ULACCUM_EPSILON__ = "0x1P-32ULK"
const m___ULACCUM_FBIT__ = 32
const m___ULACCUM_IBIT__ = 32
const m___ULACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFP-32ULK"
const m___ULACCUM_MIN__ = "0.0ULK"
const m___ULFRACT_EPSILON__ = "0x1P-32ULR"
const m___ULFRACT_FBIT__ = 32
const m___ULFRACT_IBIT__ = 0
const m___ULFRACT_MAX__ = "0XFFFFFFFFP-32ULR"
const m___ULFRACT_MIN__ = "0.0ULR"
const m___ULLACCUM_EPSILON__ = "0x1P-64ULLK"
const m___ULLACCUM_FBIT__ = 64
const m___ULLACCUM_IBIT__ = 64
const m___ULLACCUM_MAX__ = "0XFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFP-64ULLK"
const m___ULLACCUM_MIN__ = "0.0ULLK"
const m___ULLFRACT_EPSILON__ = "0x1P-64ULLR"
const m___ULLFRACT_FBIT__ = 64
const m___ULLFRACT_IBIT__ = 0
const m___ULLFRACT_MAX__ = "0XFFFFFFFFFFFFFFFFP-64ULLR"
const m___ULLFRACT_MIN__ = "0.0ULLR"
const m___UQQ_FBIT__ = 8
const m___UQQ_IBIT__ = 0
const m___USACCUM_EPSILON__ = "0x1P-8UHK"
const m___USACCUM_FBIT__ = 8
const m___USACCUM_IBIT__ = 8
const m___USACCUM_MAX__ = "0XFFFFP-8UHK"
const m___USACCUM_MIN__ = "0.0UHK"
const m___USA_FBIT__ = 16
const m___USA_IBIT__ = 16
const m___USE_TIME_BITS64 = 1
const m___USFRACT_EPSILON__ = "0x1P-8UHR"
const m___USFRACT_FBIT__ = 8
const m___USFRACT_IBIT__ = 0
const m___USFRACT_MAX__ = "0XFFP-8UHR"
const m___USFRACT_MIN__ = "0.0UHR"
const m___USQ_FBIT__ = 32
const m___USQ_IBIT__ = 0
const m___UTA_FBIT__ = 64
const m___UTA_IBIT__ = 64
const m___UTQ_FBIT__ = 128
const m___UTQ_IBIT__ = 0
const m___VERSION__ = "14.1.0 20240507 (Red Hat 14.1.0-1)"
const m___WCHAR_MAX__ = 0x7fffffff
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 0xffffffff
const m___WINT_MIN__ = 0
const m___WINT_WIDTH__ = 32
const m___WORDSIZE = 64
const m___WORDSIZE_TIME64_COMPAT32 = 0
const m___builtin_copysignq = "__builtin_copysignf128"
const m___builtin_fabsq = "__builtin_fabsf128"
const m___builtin_huge_valq = "__builtin_huge_valf128"
const m___builtin_infq = "__builtin_inff128"
const m___builtin_nanq = "__builtin_nanf128"
const m___builtin_nansq = "__builtin_nansf128"
const m___glibc_c99_flexarr_available = 1
const m___gnu_linux__ = 1
const m___inline = "inline"
const m___linux = 1
const m___linux__ = 1
const m___loongarch64 = 1
const m___loongarch__ = 1
const m___loongarch_arch = "la64v1.0"
const m___loongarch_double_float = 1
const m___loongarch_frlen = 64
const m___loongarch_grlen = 64
const m___loongarch_hard_float = 1
const m___loongarch_lp64 = 1
const m___loongarch_simd = 1
const m___loongarch_simd_width = 128
const m___loongarch_sx = 1
const m___loongarch_tune = "generic"
const m___loongarch_version_major = 1
const m___loongarch_version_minor = 0
const m___restrict = "restrict"
const m___unix = 1
const m___unix__ = 1
const m_linux = 1
const m_static_assert = "_Static_assert"
const m_unix = 1

type T__builtin_va_list = uintptr

type T__predefined_size_t = uint64

type T__predefined_wchar_t = int32

type T__predefined_ptrdiff_t = int64

type Tnlink_t = uint32

type Tblksize_t = int32

type Tsize_t = uint64

type Tssize_t = int64

type Tregister_t = int64

type Ttime_t = int64

type Tsuseconds_t = int64

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tu_int64_t = uint64

type Tmode_t = uint32

type Toff_t = int64

type Tino_t = uint64

type Tdev_t = uint64

type Tblkcnt_t = int64

type Tfsblkcnt_t = uint64

type Tfsfilcnt_t = uint64

type Ttimer_t = uintptr

type Tclockid_t = int32

type Tclock_t = int64

type Tpid_t = int32

type Tid_t = uint32

type Tuid_t = uint32

type Tgid_t = uint32

type Tkey_t = int32

type Tuseconds_t = uint32

type Tpthread_t = uintptr

type Tpthread_once_t = int32

type Tpthread_key_t = uint32

type Tpthread_spinlock_t = int32

type Tpthread_mutexattr_t = struct {
	F__attr uint32
}

type Tpthread_condattr_t = struct {
	F__attr uint32
}

type Tpthread_barrierattr_t = struct {
	F__attr uint32
}

type Tpthread_rwlockattr_t = struct {
	F__attr [2]uint32
}

type Tpthread_attr_t = struct {
	F__u struct {
		F__vi [0][14]int32
		F__s  [0][7]uint64
		F__i  [14]int32
	}
}

type Tpthread_mutex_t = struct {
	F__u struct {
		F__vi [0][10]int32
		F__p  [0][5]uintptr
		F__i  [10]int32
	}
}

type Tpthread_cond_t = struct {
	F__u struct {
		F__vi [0][12]int32
		F__p  [0][6]uintptr
		F__i  [12]int32
	}
}

type Tpthread_rwlock_t = struct {
	F__u struct {
		F__vi [0][14]int32
		F__p  [0][7]uintptr
		F__i  [14]int32
	}
}

type Tpthread_barrier_t = struct {
	F__u struct {
		F__vi [0][8]int32
		F__p  [0][4]uintptr
		F__i  [8]int32
	}
}

type Tu_int8_t = uint8

type Tu_int16_t = uint16

type Tu_int32_t = uint32

type Tcaddr_t = uintptr

type Tu_char = uint8

type Tu_short = uint16

type Tushort = uint16

type Tu_int = uint32

type Tuint = uint32

type Tu_long = uint64

type Tulong = uint64

type Tquad_t = int64

type Tu_quad_t = uint64

type Tuint16_t = uint16

type Tuint32_t = uint32

type Tuint64_t = uint64

type Ttimeval = struct {
	Ftv_sec  Ttime_t
	Ftv_usec Tsuseconds_t
}

type Ttimespec = struct {
	Ftv_sec  Ttime_t
	Ftv_nsec int64
}

type Tsigset_t = struct {
	F__bits [16]uint64
}

type T__sigset_t = Tsigset_t

type Tfd_mask = uint64

type Tfd_set = struct {
	Ffds_bits [16]uint64
}

type Tuintptr_t = uint64

type Tintptr_t = int64

type Tintmax_t = int64

type Tuint8_t = uint8

type Tuintmax_t = uint64

type Tint_fast8_t = int8

type Tint_fast64_t = int64

type Tint_least8_t = int8

type Tint_least16_t = int16

type Tint_least32_t = int32

type Tint_least64_t = int64

type Tuint_fast8_t = uint8

type Tuint_fast64_t = uint64

type Tuint_least8_t = uint8

type Tuint_least16_t = uint16

type Tuint_least32_t = uint32

type Tuint_least64_t = uint64

type Tint_fast16_t = int32

type Tint_fast32_t = int32

type Tuint_fast16_t = uint32

type Tuint_fast32_t = uint32

type TMD5_CTX = struct {
	Fstate  [4]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

type TMD5Context = TMD5_CTX

type Tlocale_t = uintptr

func _test_md5(tls *libc.TLS, digest uintptr, string1 uintptr) {
	var result [33]int8
	_ = result
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	_test_md5(tls, __ccgo_ts, __ccgo_ts+33)
	_test_md5(tls, __ccgo_ts+34, __ccgo_ts+67)
	_test_md5(tls, __ccgo_ts+71, __ccgo_ts+104)
	return 0
}

func main() {
	libc.Start(x_main)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "d41d8cd98f00b204e9800998ecf8427e\x00\x00900150983cd24fb0d6963f7d28e17f72\x00abc\x00827ccb0eea8a706c4c34a16891f84e7b\x0012345\x00"
