// Code generated for linux/ppc64le by 'gcc -no-main-minimize --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-link-errors -ignore-unsupported-alignment -ignore-link-errors -I /home/debian/src/modernc.org/builder/.exclude/modernc.org/libc/include/linux/ppc64le -I /home/debian/src/modernc.org/builder/.exclude/modernc.org/limd/include/linux/ppc64le -lbsd -DNDEBUG -mlong-double-64 -o .libs/pwcache.go pwcache.o.go -lbsd', DO NOT EDIT.

//go:build linux && ppc64le

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_BIG_ENDIAN = "__BIG_ENDIAN"
const m_BUFSIZ = 1024
const m_BYTE_ORDER = "__BYTE_ORDER"
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_FD_SETSIZE = 1024
const m_FILENAME_MAX = 4096
const m_FOPEN_MAX = 1000
const m_FPARSELN_UNESCALL = 0x0f
const m_FPARSELN_UNESCCOMM = 0x04
const m_FPARSELN_UNESCCONT = 0x02
const m_FPARSELN_UNESCESC = 0x01
const m_FPARSELN_UNESCREST = 0x08
const m_HAVE_ASPRINTF = 1
const m_HAVE_CLEARENV = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DECL_ENVIRON = 1
const m_HAVE_DIRENT_H = 1
const m_HAVE_DIRFD = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_FLOCK = 1
const m_HAVE_FOPENCOOKIE = 1
const m_HAVE_GETAUXVAL = 1
const m_HAVE_GETENTROPY = 1
const m_HAVE_GETLINE = 1
const m_HAVE_GRP_H = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_OPEN_MEMSTREAM = 1
const m_HAVE_PROGRAM_INVOCATION_SHORT_NAME = 1
const m_HAVE_PWD_H = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_EXT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYSCONF = 1
const m_HAVE_SYS_DIR_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_TYPEOF = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_VASPRINTF = 1
const m_HAVE_WCHAR_H = 1
const m_HAVE___FPURGE = 1
const m_HAVE___PROGNAME = 1
const m_HN_AUTOSCALE = 0x20
const m_HN_B = 0x04
const m_HN_DECIMAL = 0x01
const m_HN_DIVISOR_1000 = 0x08
const m_HN_GETSCALE = 0x10
const m_HN_IEC_PREFIXES = 0x10
const m_HN_NOSPACE = 0x02
const m_INT16_MAX = 0x7fff
const m_INT32_MAX = 0x7fffffff
const m_INT64_MAX = 0x7fffffffffffffff
const m_INT8_MAX = 0x7f
const m_INTMAX_MAX = "INT64_MAX"
const m_INTMAX_MIN = "INT64_MIN"
const m_INTPTR_MAX = "INT64_MAX"
const m_INTPTR_MIN = "INT64_MIN"
const m_INT_FAST16_MAX = "INT32_MAX"
const m_INT_FAST16_MIN = "INT32_MIN"
const m_INT_FAST32_MAX = "INT32_MAX"
const m_INT_FAST32_MIN = "INT32_MIN"
const m_INT_FAST64_MAX = "INT64_MAX"
const m_INT_FAST64_MIN = "INT64_MIN"
const m_INT_FAST8_MAX = "INT8_MAX"
const m_INT_FAST8_MIN = "INT8_MIN"
const m_INT_LEAST16_MAX = "INT16_MAX"
const m_INT_LEAST16_MIN = "INT16_MIN"
const m_INT_LEAST32_MAX = "INT32_MAX"
const m_INT_LEAST32_MIN = "INT32_MIN"
const m_INT_LEAST64_MAX = "INT64_MAX"
const m_INT_LEAST64_MIN = "INT64_MIN"
const m_INT_LEAST8_MAX = "INT8_MAX"
const m_INT_LEAST8_MIN = "INT8_MIN"
const m_LIBBSD_ABI_ACCMODE = 1
const m_LIBBSD_ABI_ARC4RANDOM = 1
const m_LIBBSD_ABI_ARC4RANDOM_STIR = 1
const m_LIBBSD_ABI_ASPRINTF = 0
const m_LIBBSD_ABI_BSD_GETOPT = 1
const m_LIBBSD_ABI_CLOSEFROM = 1
const m_LIBBSD_ABI_ERR = 0
const m_LIBBSD_ABI_ERRC = 1
const m_LIBBSD_ABI_EXPAND_NUMBER = 1
const m_LIBBSD_ABI_EXPLICIT_BZERO = 1
const m_LIBBSD_ABI_FGETLN = 1
const m_LIBBSD_ABI_FLOPEN = 1
const m_LIBBSD_ABI_FMTCHECK = 1
const m_LIBBSD_ABI_FPURGE = 1
const m_LIBBSD_ABI_FREEZERO = 1
const m_LIBBSD_ABI_FUNOPEN = 1
const m_LIBBSD_ABI_GETBSIZE = 1
const m_LIBBSD_ABI_GETPEEREID = 1
const m_LIBBSD_ABI_HUMANIZE_NUMBER = 1
const m_LIBBSD_ABI_ID_FROM_NAME = 1
const m_LIBBSD_ABI_INET_NET_PTON = 1
const m_LIBBSD_ABI_MD5 = 1
const m_LIBBSD_ABI_NAME_FROM_ID = 1
const m_LIBBSD_ABI_NLIST = 1
const m_LIBBSD_ABI_PIDFILE = 1
const m_LIBBSD_ABI_PROCTITLE = 1
const m_LIBBSD_ABI_PROGNAME = 1
const m_LIBBSD_ABI_PWCACHE = 1
const m_LIBBSD_ABI_READPASSPHRASE = 1
const m_LIBBSD_ABI_REALLOCARRAY = 1
const m_LIBBSD_ABI_REALLOCF = 1
const m_LIBBSD_ABI_RECALLOCARRAY = 1
const m_LIBBSD_ABI_SORT = 1
const m_LIBBSD_ABI_STRINGLIST = 1
const m_LIBBSD_ABI_STRL = 1
const m_LIBBSD_ABI_STRMODE = 1
const m_LIBBSD_ABI_STRNSTR = 1
const m_LIBBSD_ABI_STRTONUM = 1
const m_LIBBSD_ABI_STRTOX = 1
const m_LIBBSD_ABI_TIME64 = 0
const m_LIBBSD_ABI_TIMECONV = 1
const m_LIBBSD_ABI_TRANSPARENT_LIBMD = 1
const m_LIBBSD_ABI_VIS = 1
const m_LIBBSD_ABI_WCSL = 1
const m_LIBBSD_DISABLE_DEPRECATED = 1
const m_LIBBSD_OVERLAY = 1
const m_LIBBSD_SYS_HAS_TIME64 = 1
const m_LIBBSD_SYS_TIME_BITS = 64
const m_LITTLE_ENDIAN = "__LITTLE_ENDIAN"
const m_LT_OBJDIR = ".libs/"
const m_L_ctermid = 20
const m_L_cuserid = 20
const m_L_tmpnam = 20
const m_NDEBUG = 1
const m_PACKAGE = "libbsd"
const m_PACKAGE_BUGREPORT = "libbsd@lists.freedesktop.org"
const m_PACKAGE_NAME = "libbsd"
const m_PACKAGE_STRING = "libbsd 0.12.0"
const m_PACKAGE_TARNAME = "libbsd"
const m_PACKAGE_URL = ""
const m_PACKAGE_VERSION = "0.12.0"
const m_PDP_ENDIAN = "__PDP_ENDIAN"
const m_PTRDIFF_MAX = "INT64_MAX"
const m_PTRDIFF_MIN = "INT64_MIN"
const m_P_tmpdir = "/tmp"
const m_RAND_MAX = 0x7fffffff
const m_SIG_ATOMIC_MAX = "INT32_MAX"
const m_SIG_ATOMIC_MIN = "INT32_MIN"
const m_SIZEOF_TIME_T = 8
const m_SIZE_MAX = "UINT64_MAX"
const m_STATX_ALL = 0xfff
const m_STATX_ATIME = 0x20
const m_STATX_BASIC_STATS = 0x7ff
const m_STATX_BLOCKS = 0x400
const m_STATX_BTIME = 0x800
const m_STATX_CTIME = 0x80
const m_STATX_GID = 0x10
const m_STATX_INO = 0x100
const m_STATX_MODE = 2
const m_STATX_MTIME = 0x40
const m_STATX_NLINK = 4
const m_STATX_SIZE = 0x200
const m_STATX_TYPE = 1
const m_STATX_UID = 8
const m_STDC_HEADERS = 1
const m_S_IEXEC = "S_IXUSR"
const m_S_IFBLK = 0060000
const m_S_IFCHR = 0020000
const m_S_IFDIR = 0040000
const m_S_IFIFO = 0010000
const m_S_IFLNK = 0120000
const m_S_IFMT = 0170000
const m_S_IFREG = 0100000
const m_S_IFSOCK = 0140000
const m_S_IREAD = "S_IRUSR"
const m_S_IRGRP = 0040
const m_S_IROTH = 0004
const m_S_IRUSR = 0400
const m_S_IRWXG = 0070
const m_S_IRWXO = 0007
const m_S_IRWXU = 0700
const m_S_ISGID = 02000
const m_S_ISUID = 04000
const m_S_ISVTX = 01000
const m_S_IWGRP = 0020
const m_S_IWOTH = 0002
const m_S_IWRITE = "S_IWUSR"
const m_S_IWUSR = 0200
const m_S_IXGRP = 0010
const m_S_IXOTH = 0001
const m_S_IXUSR = 0100
const m_TEST_SKIP = 77
const m_TMP_MAX = 10000
const m_UINT16_MAX = 0xffff
const m_UINT32_MAX = "0xffffffffu"
const m_UINT64_MAX = "0xffffffffffffffffu"
const m_UINT8_MAX = 0xff
const m_UINTMAX_MAX = "UINT64_MAX"
const m_UINTPTR_MAX = "UINT64_MAX"
const m_UINT_FAST16_MAX = "UINT32_MAX"
const m_UINT_FAST32_MAX = "UINT32_MAX"
const m_UINT_FAST64_MAX = "UINT64_MAX"
const m_UINT_FAST8_MAX = "UINT8_MAX"
const m_UINT_LEAST16_MAX = "UINT16_MAX"
const m_UINT_LEAST32_MAX = "UINT32_MAX"
const m_UINT_LEAST64_MAX = "UINT64_MAX"
const m_UINT_LEAST8_MAX = "UINT8_MAX"
const m_UTIME_NOW = 0x3fffffff
const m_UTIME_OMIT = 0x3ffffffe
const m_VERSION = "0.12.0"
const m_WINT_MAX = "UINT32_MAX"
const m_WINT_MIN = 0
const m_WNOHANG = 1
const m_WUNTRACED = 2
const m__ALL_SOURCE = 1
const m__ARCH_PPC = 1
const m__ARCH_PPC64 = 1
const m__ARCH_PPCGR = 1
const m__ARCH_PPCSQ = 1
const m__ARCH_PWR4 = 1
const m__ARCH_PWR5 = 1
const m__ARCH_PWR5X = 1
const m__ARCH_PWR6 = 1
const m__ARCH_PWR7 = 1
const m__ARCH_PWR8 = 1
const m__CALL_ELF = 2
const m__CALL_LINUX = 1
const m__DARWIN_C_SOURCE = 1
const m__GNU_SOURCE = 1
const m__HPUX_ALT_XOPEN_SOCKET_API = 1
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__LITTLE_ENDIAN = 1
const m__LP64 = 1
const m__NETBSD_SOURCE = 1
const m__OPENBSD_SOURCE = 1
const m__POSIX_PTHREAD_SEMANTICS = 1
const m__PW_BUF_LEN = 1024
const m__STDC_PREDEF_H = 1
const m__SYS_CDEFS_H = 1
const m__TANDEM_SOURCE = 1
const m___ALTIVEC__ = 1
const m___APPLE_ALTIVEC__ = 1
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BIG_ENDIAN = 4321
const m___BUILTIN_CPU_SUPPORTS__ = 1
const m___BYTE_ORDER = 1234
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___CHAR_UNSIGNED__ = 1
const m___CMODEL_MEDIUM__ = 1
const m___CRYPTO__ = 1
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DIG__ = 15
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DEC128_EPSILON__ = 1e-33
const m___DEC128_MANT_DIG__ = 34
const m___DEC128_MAX_EXP__ = 6145
const m___DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const m___DEC128_MIN__ = 1e-6143
const m___DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const m___DEC32_EPSILON__ = 1e-6
const m___DEC32_MANT_DIG__ = 7
const m___DEC32_MAX_EXP__ = 97
const m___DEC32_MAX__ = 9.999999e96
const m___DEC32_MIN__ = 1e-95
const m___DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const m___DEC64_EPSILON__ = 1e-15
const m___DEC64_MANT_DIG__ = 16
const m___DEC64_MAX_EXP__ = 385
const m___DEC64_MAX__ = "9.999999999999999E384"
const m___DEC64_MIN__ = 1e-383
const m___DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const m___DECIMAL_DIG__ = 17
const m___DEC_EVAL_METHOD__ = 2
const m___ELF__ = 1
const m___EXTENSIONS__ = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLOAT128_TYPE__ = 1
const m___FLOAT128__ = 1
const m___FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___FLT128_DECIMAL_DIG__ = 36
const m___FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT128_DIG__ = 33
const m___FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT128_HAS_DENORM__ = 1
const m___FLT128_HAS_INFINITY__ = 1
const m___FLT128_HAS_QUIET_NAN__ = 1
const m___FLT128_MANT_DIG__ = 113
const m___FLT128_MAX_10_EXP__ = 4932
const m___FLT128_MAX_EXP__ = 16384
const m___FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT32X_DECIMAL_DIG__ = 17
const m___FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT32X_DIG__ = 15
const m___FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT32X_HAS_DENORM__ = 1
const m___FLT32X_HAS_INFINITY__ = 1
const m___FLT32X_HAS_QUIET_NAN__ = 1
const m___FLT32X_MANT_DIG__ = 53
const m___FLT32X_MAX_10_EXP__ = 308
const m___FLT32X_MAX_EXP__ = 1024
const m___FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32_DECIMAL_DIG__ = 9
const m___FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT32_DIG__ = 6
const m___FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT32_HAS_DENORM__ = 1
const m___FLT32_HAS_INFINITY__ = 1
const m___FLT32_HAS_QUIET_NAN__ = 1
const m___FLT32_MANT_DIG__ = 24
const m___FLT32_MAX_10_EXP__ = 38
const m___FLT32_MAX_EXP__ = 128
const m___FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT64X_DECIMAL_DIG__ = 36
const m___FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT64X_DIG__ = 33
const m___FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT64X_HAS_DENORM__ = 1
const m___FLT64X_HAS_INFINITY__ = 1
const m___FLT64X_HAS_QUIET_NAN__ = 1
const m___FLT64X_MANT_DIG__ = 113
const m___FLT64X_MAX_10_EXP__ = 4932
const m___FLT64X_MAX_EXP__ = 16384
const m___FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64_DECIMAL_DIG__ = 17
const m___FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT64_DIG__ = 15
const m___FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT64_HAS_DENORM__ = 1
const m___FLT64_HAS_INFINITY__ = 1
const m___FLT64_HAS_QUIET_NAN__ = 1
const m___FLT64_MANT_DIG__ = 53
const m___FLT64_MAX_10_EXP__ = 308
const m___FLT64_MAX_EXP__ = 1024
const m___FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT_EVAL_METHOD_TS_18661_3__ = 0
const m___FLT_EVAL_METHOD__ = 0
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_RADIX__ = 2
const m___FP_FAST_FMA = 1
const m___FP_FAST_FMAF = 1
const m___FP_FAST_FMAF32 = 1
const m___FP_FAST_FMAF32x = 1
const m___FP_FAST_FMAF64 = 1
const m___FP_FAST_FMAL = 1
const m___FUNCTION__ = "__func__"
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GCC_IEC_559 = 2
const m___GCC_IEC_559_COMPLEX = 2
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 1
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC__ = 10
const m___GXX_ABI_VERSION = 1014
const m___HAVE_BSWAP__ = 1
const m___HAVE_GENERIC_SELECTION = 0
const m___HAVE_SPECULATION_SAFE_VALUE = 1
const m___HTM__ = 1
const m___INT16_MAX__ = 0x7fff
const m___INT32_MAX__ = 0x7fffffff
const m___INT32_TYPE__ = "int"
const m___INT64_MAX__ = 0x7fffffffffffffff
const m___INT8_MAX__ = 0x7f
const m___INTMAX_MAX__ = 0x7fffffffffffffff
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_MAX__ = 0x7fffffffffffffff
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_MAX__ = 0x7fffffffffffffff
const m___INT_FAST16_WIDTH__ = 64
const m___INT_FAST32_MAX__ = 0x7fffffffffffffff
const m___INT_FAST32_WIDTH__ = 64
const m___INT_FAST64_MAX__ = 0x7fffffffffffffff
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_MAX__ = 0x7f
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_MAX__ = 0x7fff
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_MAX__ = 0x7fffffff
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_MAX__ = 0x7fffffffffffffff
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_MAX__ = 0x7f
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 0x7fffffff
const m___INT_WIDTH__ = 32
const m___LDBL_COMPAT = 1
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const m___LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LITTLE_ENDIAN = 1234
const m___LITTLE_ENDIAN__ = 1
const m___LONG_DOUBLE_MATH_OPTIONAL = 1
const m___LONG_DOUBLE_USES_FLOAT128 = 0
const m___LONG_LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_LONG_WIDTH__ = 64
const m___LONG_MAX = 0x7fffffffffffffff
const m___LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___NO_INLINE__ = 1
const m___NO_LONG_DOUBLE_MATH = 1
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PDP_ENDIAN = 3412
const m___PIC__ = 2
const m___PIE__ = 2
const m___POWER8_VECTOR__ = 1
const m___PPC64__ = 1
const m___PPC__ = 1
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTRDIFF_MAX__ = 0x7fffffffffffffff
const m___PTRDIFF_WIDTH__ = 64
const m___QUAD_MEMORY_ATOMIC__ = 1
const m___RECIPF__ = 1
const m___RECIP_PRECISION__ = 1
const m___RECIP__ = 1
const m___REENTRANT = 1
const m___RSQRTEF__ = 1
const m___RSQRTE__ = 1
const m___SCHAR_MAX__ = 0x7f
const m___SCHAR_WIDTH__ = 8
const m___SHRT_MAX__ = 0x7fff
const m___SHRT_WIDTH__ = 16
const m___SIG_ATOMIC_MAX__ = 0x7fffffff
const m___SIG_ATOMIC_TYPE__ = "int"
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_MAX__ = 0xffffffffffffffff
const m___SIZE_WIDTH__ = 64
const m___STDC_HOSTED__ = 1
const m___STDC_IEC_559_COMPLEX__ = 1
const m___STDC_IEC_559__ = 1
const m___STDC_ISO_10646__ = 201706
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC_WANT_IEC_60559_ATTRIBS_EXT__ = 1
const m___STDC_WANT_IEC_60559_BFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_DFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_FUNCS_EXT__ = 1
const m___STDC_WANT_IEC_60559_TYPES_EXT__ = 1
const m___STDC_WANT_LIB_EXT2__ = 1
const m___STDC_WANT_MATH_SPEC_FUNCS__ = 1
const m___STDC__ = 1
const m___STRUCT_PARM_ALIGN__ = 16
const m___TM_FENCE__ = 1
const m___UINT16_MAX__ = 0xffff
const m___UINT32_MAX__ = 0xffffffff
const m___UINT64_MAX__ = 0xffffffffffffffff
const m___UINT8_MAX__ = 0xff
const m___UINTMAX_MAX__ = 0xffffffffffffffff
const m___UINTPTR_MAX__ = 0xffffffffffffffff
const m___UINT_FAST16_MAX__ = 0xffffffffffffffff
const m___UINT_FAST32_MAX__ = 0xffffffffffffffff
const m___UINT_FAST64_MAX__ = 0xffffffffffffffff
const m___UINT_FAST8_MAX__ = 0xff
const m___UINT_LEAST16_MAX__ = 0xffff
const m___UINT_LEAST32_MAX__ = 0xffffffff
const m___UINT_LEAST64_MAX__ = 0xffffffffffffffff
const m___UINT_LEAST8_MAX__ = 0xff
const m___USE_TIME_BITS64 = 1
const m___VEC_ELEMENT_REG_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___VEC__ = 10206
const m___VERSION__ = "10.2.1 20210110"
const m___VSX__ = 1
const m___WCHAR_MAX__ = 0x7fffffff
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 0xffffffff
const m___WINT_MIN__ = 0
const m___WINT_WIDTH__ = 32
const m___WORDSIZE = 64
const m___WORDSIZE_TIME64_COMPAT32 = 1
const m___builtin_copysignq = "__builtin_copysignf128"
const m___builtin_fabsq = "__builtin_fabsf128"
const m___builtin_huge_valq = "__builtin_huge_valf128"
const m___builtin_infq = "__builtin_inff128"
const m___builtin_nanq = "__builtin_nanf128"
const m___builtin_nansq = "__builtin_nansf128"
const m___builtin_vsx_vperm = "__builtin_vec_perm"
const m___builtin_vsx_xvmaddadp = "__builtin_vsx_xvmadddp"
const m___builtin_vsx_xvmaddasp = "__builtin_vsx_xvmaddsp"
const m___builtin_vsx_xvmaddmdp = "__builtin_vsx_xvmadddp"
const m___builtin_vsx_xvmaddmsp = "__builtin_vsx_xvmaddsp"
const m___builtin_vsx_xvmsubadp = "__builtin_vsx_xvmsubdp"
const m___builtin_vsx_xvmsubasp = "__builtin_vsx_xvmsubsp"
const m___builtin_vsx_xvmsubmdp = "__builtin_vsx_xvmsubdp"
const m___builtin_vsx_xvmsubmsp = "__builtin_vsx_xvmsubsp"
const m___builtin_vsx_xvnmaddadp = "__builtin_vsx_xvnmadddp"
const m___builtin_vsx_xvnmaddasp = "__builtin_vsx_xvnmaddsp"
const m___builtin_vsx_xvnmaddmdp = "__builtin_vsx_xvnmadddp"
const m___builtin_vsx_xvnmaddmsp = "__builtin_vsx_xvnmaddsp"
const m___builtin_vsx_xvnmsubadp = "__builtin_vsx_xvnmsubdp"
const m___builtin_vsx_xvnmsubasp = "__builtin_vsx_xvnmsubsp"
const m___builtin_vsx_xvnmsubmdp = "__builtin_vsx_xvnmsubdp"
const m___builtin_vsx_xvnmsubmsp = "__builtin_vsx_xvnmsubsp"
const m___builtin_vsx_xxland = "__builtin_vec_and"
const m___builtin_vsx_xxlandc = "__builtin_vec_andc"
const m___builtin_vsx_xxlnor = "__builtin_vec_nor"
const m___builtin_vsx_xxlor = "__builtin_vec_or"
const m___builtin_vsx_xxlxor = "__builtin_vec_xor"
const m___builtin_vsx_xxsel = "__builtin_vec_sel"
const m___float128 = "__ieee128"
const m___glibc_c99_flexarr_available = 1
const m___gnu_linux__ = 1
const m___inline = "inline"
const m___linux = 1
const m___linux__ = 1
const m___pic__ = 2
const m___pie__ = 2
const m___powerpc64__ = 1
const m___powerpc__ = 1
const m___restrict = "restrict"
const m___unix = 1
const m___unix__ = 1
const m_alloca = "__builtin_alloca"
const m_linux = 1
const m_static_assert = "_Static_assert"
const m_unix = 1

type T__builtin_va_list = uintptr

type T__predefined_size_t = uint64

type T__predefined_wchar_t = int32

type T__predefined_ptrdiff_t = int64

type Tsize_t = uint64

type Tlocale_t = uintptr

type Tssize_t = int64

type Tregister_t = int64

type Ttime_t = int64

type Tsuseconds_t = int64

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tu_int64_t = uint64

type Tmode_t = uint32

type Tnlink_t = uint64

type Toff_t = int64

type Tino_t = uint64

type Tdev_t = uint64

type Tblksize_t = int64

type Tblkcnt_t = int64

type Tfsblkcnt_t = uint64

type Tfsfilcnt_t = uint64

type Ttimer_t = uintptr

type Tclockid_t = int32

type Tclock_t = int64

type Tpid_t = int32

type Tid_t = uint32

type Tuid_t = uint32

type Tgid_t = uint32

type Tkey_t = int32

type Tuseconds_t = uint32

type Tpthread_t = uintptr

type Tpthread_once_t = int32

type Tpthread_key_t = uint32

type Tpthread_spinlock_t = int32

type Tpthread_mutexattr_t = struct {
	F__attr uint32
}

type Tpthread_condattr_t = struct {
	F__attr uint32
}

type Tpthread_barrierattr_t = struct {
	F__attr uint32
}

type Tpthread_rwlockattr_t = struct {
	F__attr [2]uint32
}

type Tpthread_attr_t = struct {
	F__u struct {
		F__vi [0][14]int32
		F__s  [0][7]uint64
		F__i  [14]int32
	}
}

type Tpthread_mutex_t = struct {
	F__u struct {
		F__vi [0][10]int32
		F__p  [0][5]uintptr
		F__i  [10]int32
	}
}

type Tpthread_cond_t = struct {
	F__u struct {
		F__vi [0][12]int32
		F__p  [0][6]uintptr
		F__i  [12]int32
	}
}

type Tpthread_rwlock_t = struct {
	F__u struct {
		F__vi [0][14]int32
		F__p  [0][7]uintptr
		F__i  [14]int32
	}
}

type Tpthread_barrier_t = struct {
	F__u struct {
		F__vi [0][8]int32
		F__p  [0][4]uintptr
		F__i  [8]int32
	}
}

type Tu_int8_t = uint8

type Tu_int16_t = uint16

type Tu_int32_t = uint32

type Tcaddr_t = uintptr

type Tu_char = uint8

type Tu_short = uint16

type Tushort = uint16

type Tu_int = uint32

type Tuint = uint32

type Tu_long = uint64

type Tulong = uint64

type Tquad_t = int64

type Tu_quad_t = uint64

type Tuint16_t = uint16

type Tuint32_t = uint32

type Tuint64_t = uint64

type Ttimeval = struct {
	Ftv_sec  Ttime_t
	Ftv_usec Tsuseconds_t
}

type Ttimespec = struct {
	Ftv_sec  Ttime_t
	Ftv_nsec int64
}

type Tsigset_t = struct {
	F__bits [16]uint64
}

type T__sigset_t = Tsigset_t

type Tfd_mask = uint64

type Tfd_set = struct {
	Ffds_bits [16]uint64
}

type Twchar_t = int32

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tuintptr_t = uint64

type Tintptr_t = int64

type Tintmax_t = int64

type Tuint8_t = uint8

type Tuintmax_t = uint64

type Tint_fast8_t = int8

type Tint_fast64_t = int64

type Tint_least8_t = int8

type Tint_least16_t = int16

type Tint_least32_t = int32

type Tint_least64_t = int64

type Tuint_fast8_t = uint8

type Tuint_fast64_t = uint64

type Tuint_least8_t = uint8

type Tuint_least16_t = uint16

type Tuint_least32_t = uint32

type Tuint_least64_t = uint64

type Tint_fast16_t = int32

type Tint_fast32_t = int32

type Tuint_fast16_t = uint32

type Tuint_fast32_t = uint32

type Tva_list = uintptr

type T__isoc_va_list = uintptr

type Tfpos_t = struct {
	F__lldata [0]int64
	F__align  [0]float64
	F__opaque [16]uint8
}

type T_G_fpos64_t = Tfpos_t

type Tcookie_io_functions_t = struct {
	Fread   uintptr
	Fwrite  uintptr
	Fseek   uintptr
	Fclose1 uintptr
}

type T_IO_cookie_io_functions_t = Tcookie_io_functions_t

type Tstat = struct {
	Fst_dev     Tdev_t
	Fst_ino     Tino_t
	Fst_nlink   Tnlink_t
	Fst_mode    Tmode_t
	Fst_uid     Tuid_t
	Fst_gid     Tgid_t
	Fst_rdev    Tdev_t
	Fst_size    Toff_t
	Fst_blksize Tblksize_t
	Fst_blocks  Tblkcnt_t
	Fst_atim    Ttimespec
	Fst_mtim    Ttimespec
	Fst_ctim    Ttimespec
	F__unused   [3]uint64
}

type Tstatx_timestamp = struct {
	Ftv_sec  Tint64_t
	Ftv_nsec Tuint32_t
	F__pad   Tuint32_t
}

type Tstatx = struct {
	Fstx_mask            Tuint32_t
	Fstx_blksize         Tuint32_t
	Fstx_attributes      Tuint64_t
	Fstx_nlink           Tuint32_t
	Fstx_uid             Tuint32_t
	Fstx_gid             Tuint32_t
	Fstx_mode            Tuint16_t
	F__pad0              [1]Tuint16_t
	Fstx_ino             Tuint64_t
	Fstx_size            Tuint64_t
	Fstx_blocks          Tuint64_t
	Fstx_attributes_mask Tuint64_t
	Fstx_atime           Tstatx_timestamp
	Fstx_btime           Tstatx_timestamp
	Fstx_ctime           Tstatx_timestamp
	Fstx_mtime           Tstatx_timestamp
	Fstx_rdev_major      Tuint32_t
	Fstx_rdev_minor      Tuint32_t
	Fstx_dev_major       Tuint32_t
	Fstx_dev_minor       Tuint32_t
	F__pad1              [14]Tuint64_t
}

type Tpasswd = struct {
	Fpw_name   uintptr
	Fpw_passwd uintptr
	Fpw_uid    Tuid_t
	Fpw_gid    Tgid_t
	Fpw_gecos  uintptr
	Fpw_dir    uintptr
	Fpw_shell  uintptr
}

type Tgroup = struct {
	Fgr_name   uintptr
	Fgr_passwd uintptr
	Fgr_gid    Tgid_t
	Fgr_mem    uintptr
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	var gid Tgid_t
	var gname, gr, pw, uname uintptr
	var uid Tuid_t
	_, _, _, _, _, _ = gid, gname, gr, pw, uid, uname
	/* Do not hardcode user or group names. */
	pw = libc.Xgetpwuid(tls, uint32(0))
	if pw == libc.UintptrFromInt32(0) {
		return int32(m_TEST_SKIP)
	}
	uname = libc.Xstrdup(tls, (*Tpasswd)(unsafe.Pointer(pw)).Fpw_name)
	gr = libc.Xgetgrgid(tls, uint32(0))
	if gr == libc.UintptrFromInt32(0) {
		libc.Xfree(tls, uname)
		return int32(m_TEST_SKIP)
	}
	gname = libc.Xstrdup(tls, (*Tgroup)(unsafe.Pointer(gr)).Fgr_name)
	/* Test the functions. */
	libc.Xfree(tls, uname)
	libc.Xfree(tls, gname)
	return 0
}

func main() {
	libc.Start(x_main)
}
