// Code generated for linux/amd64 by 'gcc -no-main-minimize --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-link-errors -ignore-unsupported-alignment -ignore-link-errors -I /home/jnml/src/modernc.org/builder/.exclude/modernc.org/libc/include/linux/amd64 -I /home/jnml/src/modernc.org/builder/.exclude/modernc.org/limd/include/linux/amd64 -lbsd -DNDEBUG -mlong-double-64 -o .libs/explicit_bzero.go explicit_bzero.o.go -lbsd', DO NOT EDIT.

//go:build linux && amd64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libbsd"
	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_BIG_ENDIAN = "__BIG_ENDIAN"
const m_BUFSIZ = 1024
const m_BUS_ADRALN = 1
const m_BUS_ADRERR = 2
const m_BUS_MCEERR_AO = 5
const m_BUS_MCEERR_AR = 4
const m_BUS_OBJERR = 3
const m_BYTE_ORDER = "__BYTE_ORDER"
const m_CLD_CONTINUED = 6
const m_CLD_DUMPED = 3
const m_CLD_EXITED = 1
const m_CLD_KILLED = 2
const m_CLD_STOPPED = 5
const m_CLD_TRAPPED = 4
const m_E2BIG = 7
const m_EACCES = 13
const m_EADDRINUSE = 98
const m_EADDRNOTAVAIL = 99
const m_EADV = 68
const m_EAFNOSUPPORT = 97
const m_EAGAIN = 11
const m_EALREADY = 114
const m_EBADE = 52
const m_EBADF = 9
const m_EBADFD = 77
const m_EBADMSG = 74
const m_EBADR = 53
const m_EBADRQC = 56
const m_EBADSLT = 57
const m_EBFONT = 59
const m_EBUSY = 16
const m_ECANCELED = 125
const m_ECHILD = 10
const m_ECHRNG = 44
const m_ECOMM = 70
const m_ECONNABORTED = 103
const m_ECONNREFUSED = 111
const m_ECONNRESET = 104
const m_EDEADLK = 35
const m_EDEADLOCK = "EDEADLK"
const m_EDESTADDRREQ = 89
const m_EDOM = 33
const m_EDOTDOT = 73
const m_EDQUOT = 122
const m_EEXIST = 17
const m_EFAULT = 14
const m_EFBIG = 27
const m_EHOSTDOWN = 112
const m_EHOSTUNREACH = 113
const m_EHWPOISON = 133
const m_EIDRM = 43
const m_EILSEQ = 84
const m_EINPROGRESS = 115
const m_EINTR = 4
const m_EINVAL = 22
const m_EIO = 5
const m_EISCONN = 106
const m_EISDIR = 21
const m_EISNAM = 120
const m_EKEYEXPIRED = 127
const m_EKEYREJECTED = 129
const m_EKEYREVOKED = 128
const m_EL2HLT = 51
const m_EL2NSYNC = 45
const m_EL3HLT = 46
const m_EL3RST = 47
const m_ELIBACC = 79
const m_ELIBBAD = 80
const m_ELIBEXEC = 83
const m_ELIBMAX = 82
const m_ELIBSCN = 81
const m_ELNRNG = 48
const m_ELOOP = 40
const m_EMEDIUMTYPE = 124
const m_EMFILE = 24
const m_EMLINK = 31
const m_EMSGSIZE = 90
const m_EMULTIHOP = 72
const m_ENAMETOOLONG = 36
const m_ENAVAIL = 119
const m_ENETDOWN = 100
const m_ENETRESET = 102
const m_ENETUNREACH = 101
const m_ENFILE = 23
const m_ENOANO = 55
const m_ENOBUFS = 105
const m_ENOCSI = 50
const m_ENODATA = 61
const m_ENODEV = 19
const m_ENOENT = 2
const m_ENOEXEC = 8
const m_ENOKEY = 126
const m_ENOLCK = 37
const m_ENOLINK = 67
const m_ENOMEDIUM = 123
const m_ENOMEM = 12
const m_ENOMSG = 42
const m_ENONET = 64
const m_ENOPKG = 65
const m_ENOPROTOOPT = 92
const m_ENOSPC = 28
const m_ENOSR = 63
const m_ENOSTR = 60
const m_ENOSYS = 38
const m_ENOTBLK = 15
const m_ENOTCONN = 107
const m_ENOTDIR = 20
const m_ENOTEMPTY = 39
const m_ENOTNAM = 118
const m_ENOTRECOVERABLE = 131
const m_ENOTSOCK = 88
const m_ENOTSUP = "EOPNOTSUPP"
const m_ENOTTY = 25
const m_ENOTUNIQ = 76
const m_ENXIO = 6
const m_EOPNOTSUPP = 95
const m_EOVERFLOW = 75
const m_EOWNERDEAD = 130
const m_EPERM = 1
const m_EPFNOSUPPORT = 96
const m_EPIPE = 32
const m_EPROTO = 71
const m_EPROTONOSUPPORT = 93
const m_EPROTOTYPE = 91
const m_ERANGE = 34
const m_EREMCHG = 78
const m_EREMOTE = 66
const m_EREMOTEIO = 121
const m_ERESTART = 85
const m_ERFKILL = 132
const m_EROFS = 30
const m_ESHUTDOWN = 108
const m_ESOCKTNOSUPPORT = 94
const m_ESPIPE = 29
const m_ESRCH = 3
const m_ESRMNT = 69
const m_ESTALE = 116
const m_ESTRPIPE = 86
const m_ETIME = 62
const m_ETIMEDOUT = 110
const m_ETOOMANYREFS = 109
const m_ETXTBSY = 26
const m_EUCLEAN = 117
const m_EUNATCH = 49
const m_EUSERS = 87
const m_EWOULDBLOCK = "EAGAIN"
const m_EXDEV = 18
const m_EXFULL = 54
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_FD_SETSIZE = 1024
const m_FILENAME_MAX = 4096
const m_FOPEN_MAX = 1000
const m_FPARSELN_UNESCALL = 0x0f
const m_FPARSELN_UNESCCOMM = 0x04
const m_FPARSELN_UNESCCONT = 0x02
const m_FPARSELN_UNESCESC = 0x01
const m_FPARSELN_UNESCREST = 0x08
const m_FPE_FLTDIV = 3
const m_FPE_FLTINV = 7
const m_FPE_FLTOVF = 4
const m_FPE_FLTRES = 6
const m_FPE_FLTSUB = 8
const m_FPE_FLTUND = 5
const m_FPE_INTDIV = 1
const m_FPE_INTOVF = 2
const m_F_LOCK = 1
const m_F_OK = 0
const m_F_TEST = 3
const m_F_TLOCK = 2
const m_F_ULOCK = 0
const m_HAVE_CLEARENV = 1
const m_HAVE_CONFIG_H = 1
const m_HAVE_DIRENT_H = 1
const m_HAVE_DIRFD = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_FOPENCOOKIE = 1
const m_HAVE_GETAUXVAL = 1
const m_HAVE_GETENTROPY = 1
const m_HAVE_GETLINE = 1
const m_HAVE_GRP_H = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_PROGRAM_INVOCATION_SHORT_NAME = 1
const m_HAVE_PWD_H = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYSCONF = 1
const m_HAVE_SYS_DIR_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_TYPEOF = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_WCHAR_H = 1
const m_HAVE___FPURGE = 1
const m_HAVE___PROGNAME = 1
const m_HN_AUTOSCALE = 0x20
const m_HN_B = 0x04
const m_HN_DECIMAL = 0x01
const m_HN_DIVISOR_1000 = 0x08
const m_HN_GETSCALE = 0x10
const m_HN_IEC_PREFIXES = 0x10
const m_HN_NOSPACE = 0x02
const m_ILL_BADSTK = 8
const m_ILL_COPROC = 7
const m_ILL_ILLADR = 3
const m_ILL_ILLOPC = 1
const m_ILL_ILLOPN = 2
const m_ILL_ILLTRP = 4
const m_ILL_PRVOPC = 5
const m_ILL_PRVREG = 6
const m_INT16_MAX = 0x7fff
const m_INT32_MAX = 0x7fffffff
const m_INT64_MAX = 0x7fffffffffffffff
const m_INT8_MAX = 0x7f
const m_INTMAX_MAX = "INT64_MAX"
const m_INTMAX_MIN = "INT64_MIN"
const m_INTPTR_MAX = "INT64_MAX"
const m_INTPTR_MIN = "INT64_MIN"
const m_INT_FAST16_MAX = "INT32_MAX"
const m_INT_FAST16_MIN = "INT32_MIN"
const m_INT_FAST32_MAX = "INT32_MAX"
const m_INT_FAST32_MIN = "INT32_MIN"
const m_INT_FAST64_MAX = "INT64_MAX"
const m_INT_FAST64_MIN = "INT64_MIN"
const m_INT_FAST8_MAX = "INT8_MAX"
const m_INT_FAST8_MIN = "INT8_MIN"
const m_INT_LEAST16_MAX = "INT16_MAX"
const m_INT_LEAST16_MIN = "INT16_MIN"
const m_INT_LEAST32_MAX = "INT32_MAX"
const m_INT_LEAST32_MIN = "INT32_MIN"
const m_INT_LEAST64_MAX = "INT64_MAX"
const m_INT_LEAST64_MIN = "INT64_MIN"
const m_INT_LEAST8_MAX = "INT8_MAX"
const m_INT_LEAST8_MIN = "INT8_MIN"
const m_LIBBSD_DISABLE_DEPRECATED = 1
const m_LIBBSD_OVERLAY = 1
const m_LITTLE_ENDIAN = "__LITTLE_ENDIAN"
const m_LT_OBJDIR = ".libs/"
const m_L_INCR = 1
const m_L_SET = 0
const m_L_XTND = 2
const m_L_ctermid = 20
const m_L_cuserid = 20
const m_L_tmpnam = 20
const m_MINSIGSTKSZ = 2048
const m_NDEBUG = 1
const m_NSIG = "_NSIG"
const m_PACKAGE = "libbsd"
const m_PACKAGE_BUGREPORT = "libbsd@lists.freedesktop.org"
const m_PACKAGE_NAME = "libbsd"
const m_PACKAGE_STRING = "libbsd 0.11.7"
const m_PACKAGE_TARNAME = "libbsd"
const m_PACKAGE_URL = ""
const m_PACKAGE_VERSION = "0.11.7"
const m_PDP_ENDIAN = "__PDP_ENDIAN"
const m_POLL_ERR = 4
const m_POLL_HUP = 6
const m_POLL_IN = 1
const m_POLL_MSG = 3
const m_POLL_OUT = 2
const m_POLL_PRI = 5
const m_POSIX_CLOSE_RESTART = 0
const m_PTRDIFF_MAX = "INT64_MAX"
const m_PTRDIFF_MIN = "INT64_MIN"
const m_P_tmpdir = "/tmp"
const m_RAND_MAX = 0x7fffffff
const m_R_OK = 4
const m_SA_EXPOSE_TAGBITS = 0x00000800
const m_SA_NOCLDSTOP = 1
const m_SA_NOCLDWAIT = 2
const m_SA_NODEFER = 0x40000000
const m_SA_NOMASK = "SA_NODEFER"
const m_SA_ONESHOT = "SA_RESETHAND"
const m_SA_ONSTACK = 134217728
const m_SA_RESETHAND = 0x80000000
const m_SA_RESTART = 0x10000000
const m_SA_RESTORER = 0x04000000
const m_SA_SIGINFO = 4
const m_SA_UNSUPPORTED = 0x00000400
const m_SEEK_DATA = 3
const m_SEEK_HOLE = 4
const m_SEGV_ACCERR = 2
const m_SEGV_BNDERR = 3
const m_SEGV_MAPERR = 1
const m_SEGV_MTEAERR = 8
const m_SEGV_MTESERR = 9
const m_SEGV_PKUERR = 4
const m_SIGABRT = 6
const m_SIGALRM = 14
const m_SIGBUS = 7
const m_SIGCHLD = 17
const m_SIGCONT = 18
const m_SIGEV_NONE = 1
const m_SIGEV_SIGNAL = 0
const m_SIGEV_THREAD = 2
const m_SIGEV_THREAD_ID = 4
const m_SIGFPE = 8
const m_SIGHUP = 1
const m_SIGILL = 4
const m_SIGINT = 2
const m_SIGIO = 29
const m_SIGIOT = "SIGABRT"
const m_SIGKILL = 9
const m_SIGPIPE = 13
const m_SIGPOLL = 29
const m_SIGPROF = 27
const m_SIGPWR = 30
const m_SIGQUIT = 3
const m_SIGSEGV = 11
const m_SIGSTKFLT = 16
const m_SIGSTKSZ = 8192
const m_SIGSTOP = 19
const m_SIGSYS = 31
const m_SIGTERM = 15
const m_SIGTRAP = 5
const m_SIGTSTP = 20
const m_SIGTTIN = 21
const m_SIGTTOU = 22
const m_SIGUNUSED = "SIGSYS"
const m_SIGURG = 23
const m_SIGUSR1 = 10
const m_SIGUSR2 = 12
const m_SIGVTALRM = 26
const m_SIGWINCH = 28
const m_SIGXCPU = 24
const m_SIGXFSZ = 25
const m_SIG_ATOMIC_MAX = "INT32_MAX"
const m_SIG_ATOMIC_MIN = "INT32_MIN"
const m_SIG_BLOCK = 0
const m_SIG_SETMASK = 2
const m_SIG_UNBLOCK = 1
const m_SIZE_MAX = "UINT64_MAX"
const m_SI_KERNEL = 128
const m_SI_USER = 0
const m_SS_DISABLE = 2
const m_SS_FLAG_BITS = "SS_AUTODISARM"
const m_SS_ONSTACK = 1
const m_STATX_ALL = 0xfff
const m_STATX_ATIME = 0x20
const m_STATX_BASIC_STATS = 0x7ff
const m_STATX_BLOCKS = 0x400
const m_STATX_BTIME = 0x800
const m_STATX_CTIME = 0x80
const m_STATX_GID = 0x10
const m_STATX_INO = 0x100
const m_STATX_MODE = 2
const m_STATX_MTIME = 0x40
const m_STATX_NLINK = 4
const m_STATX_SIZE = 0x200
const m_STATX_TYPE = 1
const m_STATX_UID = 8
const m_STDC_HEADERS = 1
const m_STDERR_FILENO = 2
const m_STDIN_FILENO = 0
const m_STDOUT_FILENO = 1
const m_SYS_SECCOMP = 1
const m_SYS_USER_DISPATCH = 2
const m_S_IEXEC = "S_IXUSR"
const m_S_IFBLK = 0060000
const m_S_IFCHR = 0020000
const m_S_IFDIR = 0040000
const m_S_IFIFO = 0010000
const m_S_IFLNK = 0120000
const m_S_IFMT = 0170000
const m_S_IFREG = 0100000
const m_S_IFSOCK = 0140000
const m_S_IREAD = "S_IRUSR"
const m_S_IRGRP = 0040
const m_S_IROTH = 0004
const m_S_IRUSR = 0400
const m_S_IRWXG = 0070
const m_S_IRWXO = 0007
const m_S_IRWXU = 0700
const m_S_ISGID = 02000
const m_S_ISTXT = "S_ISVTX"
const m_S_ISUID = 04000
const m_S_ISVTX = 01000
const m_S_IWGRP = 0020
const m_S_IWOTH = 0002
const m_S_IWRITE = "S_IWUSR"
const m_S_IWUSR = 0200
const m_S_IXGRP = 0010
const m_S_IXOTH = 0001
const m_S_IXUSR = 0100
const m_TMP_MAX = 10000
const m_TRAP_BRANCH = 3
const m_TRAP_BRKPT = 1
const m_TRAP_HWBKPT = 4
const m_TRAP_TRACE = 2
const m_TRAP_UNK = 5
const m_UINT16_MAX = 0xffff
const m_UINT32_MAX = "0xffffffffu"
const m_UINT64_MAX = "0xffffffffffffffffu"
const m_UINT8_MAX = 0xff
const m_UINTMAX_MAX = "UINT64_MAX"
const m_UINTPTR_MAX = "UINT64_MAX"
const m_UINT_FAST16_MAX = "UINT32_MAX"
const m_UINT_FAST32_MAX = "UINT32_MAX"
const m_UINT_FAST64_MAX = "UINT64_MAX"
const m_UINT_FAST8_MAX = "UINT8_MAX"
const m_UINT_LEAST16_MAX = "UINT16_MAX"
const m_UINT_LEAST32_MAX = "UINT32_MAX"
const m_UINT_LEAST64_MAX = "UINT64_MAX"
const m_UINT_LEAST8_MAX = "UINT8_MAX"
const m_UTIME_NOW = 0x3fffffff
const m_UTIME_OMIT = 0x3ffffffe
const m_VERSION = "0.11.7"
const m_WINT_MAX = "UINT32_MAX"
const m_WINT_MIN = 0
const m_WNOHANG = 1
const m_WUNTRACED = 2
const m_W_OK = 2
const m_X_OK = 1
const m__ALL_SOURCE = 1
const m__CS_GNU_LIBC_VERSION = 2
const m__CS_GNU_LIBPTHREAD_VERSION = 3
const m__CS_PATH = 0
const m__CS_POSIX_V5_WIDTH_RESTRICTED_ENVS = 4
const m__CS_POSIX_V6_ILP32_OFF32_CFLAGS = 1116
const m__CS_POSIX_V6_ILP32_OFF32_LDFLAGS = 1117
const m__CS_POSIX_V6_ILP32_OFF32_LIBS = 1118
const m__CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = 1119
const m__CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = 1120
const m__CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = 1121
const m__CS_POSIX_V6_ILP32_OFFBIG_LIBS = 1122
const m__CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = 1123
const m__CS_POSIX_V6_LP64_OFF64_CFLAGS = 1124
const m__CS_POSIX_V6_LP64_OFF64_LDFLAGS = 1125
const m__CS_POSIX_V6_LP64_OFF64_LIBS = 1126
const m__CS_POSIX_V6_LP64_OFF64_LINTFLAGS = 1127
const m__CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = 1128
const m__CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = 1129
const m__CS_POSIX_V6_LPBIG_OFFBIG_LIBS = 1130
const m__CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = 1131
const m__CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = 1
const m__CS_POSIX_V7_ILP32_OFF32_CFLAGS = 1132
const m__CS_POSIX_V7_ILP32_OFF32_LDFLAGS = 1133
const m__CS_POSIX_V7_ILP32_OFF32_LIBS = 1134
const m__CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = 1135
const m__CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = 1136
const m__CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = 1137
const m__CS_POSIX_V7_ILP32_OFFBIG_LIBS = 1138
const m__CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = 1139
const m__CS_POSIX_V7_LP64_OFF64_CFLAGS = 1140
const m__CS_POSIX_V7_LP64_OFF64_LDFLAGS = 1141
const m__CS_POSIX_V7_LP64_OFF64_LIBS = 1142
const m__CS_POSIX_V7_LP64_OFF64_LINTFLAGS = 1143
const m__CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = 1144
const m__CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = 1145
const m__CS_POSIX_V7_LPBIG_OFFBIG_LIBS = 1146
const m__CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = 1147
const m__CS_POSIX_V7_THREADS_CFLAGS = 1150
const m__CS_POSIX_V7_THREADS_LDFLAGS = 1151
const m__CS_POSIX_V7_WIDTH_RESTRICTED_ENVS = 5
const m__CS_V6_ENV = 1148
const m__CS_V7_ENV = 1149
const m__DARWIN_C_SOURCE = 1
const m__GNU_SOURCE = 1
const m__HPUX_ALT_XOPEN_SOCKET_API = 1
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__LP64 = 1
const m__NETBSD_SOURCE = 1
const m__NSIG = 65
const m__OPENBSD_SOURCE = 1
const m__PC_2_SYMLINKS = 20
const m__PC_ALLOC_SIZE_MIN = 18
const m__PC_ASYNC_IO = 10
const m__PC_CHOWN_RESTRICTED = 6
const m__PC_FILESIZEBITS = 13
const m__PC_LINK_MAX = 0
const m__PC_MAX_CANON = 1
const m__PC_MAX_INPUT = 2
const m__PC_NAME_MAX = 3
const m__PC_NO_TRUNC = 7
const m__PC_PATH_MAX = 4
const m__PC_PIPE_BUF = 5
const m__PC_PRIO_IO = 11
const m__PC_REC_INCR_XFER_SIZE = 14
const m__PC_REC_MAX_XFER_SIZE = 15
const m__PC_REC_MIN_XFER_SIZE = 16
const m__PC_REC_XFER_ALIGN = 17
const m__PC_SOCK_MAXBUF = 12
const m__PC_SYMLINK_MAX = 19
const m__PC_SYNC_IO = 9
const m__PC_VDISABLE = 8
const m__POSIX2_C_BIND = "_POSIX_VERSION"
const m__POSIX2_VERSION = "_POSIX_VERSION"
const m__POSIX_ADVISORY_INFO = "_POSIX_VERSION"
const m__POSIX_ASYNCHRONOUS_IO = "_POSIX_VERSION"
const m__POSIX_BARRIERS = "_POSIX_VERSION"
const m__POSIX_CHOWN_RESTRICTED = 1
const m__POSIX_CLOCK_SELECTION = "_POSIX_VERSION"
const m__POSIX_CPUTIME = "_POSIX_VERSION"
const m__POSIX_FSYNC = "_POSIX_VERSION"
const m__POSIX_IPV6 = "_POSIX_VERSION"
const m__POSIX_JOB_CONTROL = 1
const m__POSIX_MAPPED_FILES = "_POSIX_VERSION"
const m__POSIX_MEMLOCK = "_POSIX_VERSION"
const m__POSIX_MEMLOCK_RANGE = "_POSIX_VERSION"
const m__POSIX_MEMORY_PROTECTION = "_POSIX_VERSION"
const m__POSIX_MESSAGE_PASSING = "_POSIX_VERSION"
const m__POSIX_MONOTONIC_CLOCK = "_POSIX_VERSION"
const m__POSIX_NO_TRUNC = 1
const m__POSIX_PTHREAD_SEMANTICS = 1
const m__POSIX_RAW_SOCKETS = "_POSIX_VERSION"
const m__POSIX_READER_WRITER_LOCKS = "_POSIX_VERSION"
const m__POSIX_REALTIME_SIGNALS = "_POSIX_VERSION"
const m__POSIX_REGEXP = 1
const m__POSIX_SAVED_IDS = 1
const m__POSIX_SEMAPHORES = "_POSIX_VERSION"
const m__POSIX_SHARED_MEMORY_OBJECTS = "_POSIX_VERSION"
const m__POSIX_SHELL = 1
const m__POSIX_SPAWN = "_POSIX_VERSION"
const m__POSIX_SPIN_LOCKS = "_POSIX_VERSION"
const m__POSIX_THREADS = "_POSIX_VERSION"
const m__POSIX_THREAD_ATTR_STACKADDR = "_POSIX_VERSION"
const m__POSIX_THREAD_ATTR_STACKSIZE = "_POSIX_VERSION"
const m__POSIX_THREAD_CPUTIME = "_POSIX_VERSION"
const m__POSIX_THREAD_PRIORITY_SCHEDULING = "_POSIX_VERSION"
const m__POSIX_THREAD_PROCESS_SHARED = "_POSIX_VERSION"
const m__POSIX_THREAD_SAFE_FUNCTIONS = "_POSIX_VERSION"
const m__POSIX_TIMEOUTS = "_POSIX_VERSION"
const m__POSIX_TIMERS = "_POSIX_VERSION"
const m__POSIX_V6_LP64_OFF64 = 1
const m__POSIX_V7_LP64_OFF64 = 1
const m__POSIX_VDISABLE = 0
const m__POSIX_VERSION = 200809
const m__SC_2_CHAR_TERM = 95
const m__SC_2_C_BIND = 47
const m__SC_2_C_DEV = 48
const m__SC_2_FORT_DEV = 49
const m__SC_2_FORT_RUN = 50
const m__SC_2_LOCALEDEF = 52
const m__SC_2_PBS = 168
const m__SC_2_PBS_ACCOUNTING = 169
const m__SC_2_PBS_CHECKPOINT = 175
const m__SC_2_PBS_LOCATE = 170
const m__SC_2_PBS_MESSAGE = 171
const m__SC_2_PBS_TRACK = 172
const m__SC_2_SW_DEV = 51
const m__SC_2_UPE = 97
const m__SC_2_VERSION = 46
const m__SC_ADVISORY_INFO = 132
const m__SC_AIO_LISTIO_MAX = 23
const m__SC_AIO_MAX = 24
const m__SC_AIO_PRIO_DELTA_MAX = 25
const m__SC_ARG_MAX = 0
const m__SC_ASYNCHRONOUS_IO = 12
const m__SC_ATEXIT_MAX = 87
const m__SC_AVPHYS_PAGES = 86
const m__SC_BARRIERS = 133
const m__SC_BC_BASE_MAX = 36
const m__SC_BC_DIM_MAX = 37
const m__SC_BC_SCALE_MAX = 38
const m__SC_BC_STRING_MAX = 39
const m__SC_CHILD_MAX = 1
const m__SC_CLK_TCK = 2
const m__SC_CLOCK_SELECTION = 137
const m__SC_COLL_WEIGHTS_MAX = 40
const m__SC_CPUTIME = 138
const m__SC_DELAYTIMER_MAX = 26
const m__SC_EXPR_NEST_MAX = 42
const m__SC_FSYNC = 15
const m__SC_GETGR_R_SIZE_MAX = 69
const m__SC_GETPW_R_SIZE_MAX = 70
const m__SC_HOST_NAME_MAX = 180
const m__SC_IOV_MAX = 60
const m__SC_IPV6 = 235
const m__SC_JOB_CONTROL = 7
const m__SC_LINE_MAX = 43
const m__SC_LOGIN_NAME_MAX = 71
const m__SC_MAPPED_FILES = 16
const m__SC_MEMLOCK = 17
const m__SC_MEMLOCK_RANGE = 18
const m__SC_MEMORY_PROTECTION = 19
const m__SC_MESSAGE_PASSING = 20
const m__SC_MINSIGSTKSZ = 249
const m__SC_MONOTONIC_CLOCK = 149
const m__SC_MQ_OPEN_MAX = 27
const m__SC_MQ_PRIO_MAX = 28
const m__SC_NGROUPS_MAX = 3
const m__SC_NPROCESSORS_CONF = 83
const m__SC_NPROCESSORS_ONLN = 84
const m__SC_NZERO = 109
const m__SC_OPEN_MAX = 4
const m__SC_PAGESIZE = 30
const m__SC_PAGE_SIZE = 30
const m__SC_PASS_MAX = 88
const m__SC_PHYS_PAGES = 85
const m__SC_PRIORITIZED_IO = 13
const m__SC_PRIORITY_SCHEDULING = 10
const m__SC_RAW_SOCKETS = 236
const m__SC_READER_WRITER_LOCKS = 153
const m__SC_REALTIME_SIGNALS = 9
const m__SC_REGEXP = 155
const m__SC_RE_DUP_MAX = 44
const m__SC_RTSIG_MAX = 31
const m__SC_SAVED_IDS = 8
const m__SC_SEMAPHORES = 21
const m__SC_SEM_NSEMS_MAX = 32
const m__SC_SEM_VALUE_MAX = 33
const m__SC_SHARED_MEMORY_OBJECTS = 22
const m__SC_SHELL = 157
const m__SC_SIGQUEUE_MAX = 34
const m__SC_SIGSTKSZ = 250
const m__SC_SPAWN = 159
const m__SC_SPIN_LOCKS = 154
const m__SC_SPORADIC_SERVER = 160
const m__SC_SS_REPL_MAX = 241
const m__SC_STREAMS = 174
const m__SC_STREAM_MAX = 5
const m__SC_SYMLOOP_MAX = 173
const m__SC_SYNCHRONIZED_IO = 14
const m__SC_THREADS = 67
const m__SC_THREAD_ATTR_STACKADDR = 77
const m__SC_THREAD_ATTR_STACKSIZE = 78
const m__SC_THREAD_CPUTIME = 139
const m__SC_THREAD_DESTRUCTOR_ITERATIONS = 73
const m__SC_THREAD_KEYS_MAX = 74
const m__SC_THREAD_PRIORITY_SCHEDULING = 79
const m__SC_THREAD_PRIO_INHERIT = 80
const m__SC_THREAD_PRIO_PROTECT = 81
const m__SC_THREAD_PROCESS_SHARED = 82
const m__SC_THREAD_ROBUST_PRIO_INHERIT = 247
const m__SC_THREAD_ROBUST_PRIO_PROTECT = 248
const m__SC_THREAD_SAFE_FUNCTIONS = 68
const m__SC_THREAD_SPORADIC_SERVER = 161
const m__SC_THREAD_STACK_MIN = 75
const m__SC_THREAD_THREADS_MAX = 76
const m__SC_TIMEOUTS = 164
const m__SC_TIMERS = 11
const m__SC_TIMER_MAX = 35
const m__SC_TRACE = 181
const m__SC_TRACE_EVENT_FILTER = 182
const m__SC_TRACE_EVENT_NAME_MAX = 242
const m__SC_TRACE_INHERIT = 183
const m__SC_TRACE_LOG = 184
const m__SC_TRACE_NAME_MAX = 243
const m__SC_TRACE_SYS_MAX = 244
const m__SC_TRACE_USER_EVENT_MAX = 245
const m__SC_TTY_NAME_MAX = 72
const m__SC_TYPED_MEMORY_OBJECTS = 165
const m__SC_TZNAME_MAX = 6
const m__SC_UIO_MAXIOV = 60
const m__SC_V6_ILP32_OFF32 = 176
const m__SC_V6_ILP32_OFFBIG = 177
const m__SC_V6_LP64_OFF64 = 178
const m__SC_V6_LPBIG_OFFBIG = 179
const m__SC_V7_ILP32_OFF32 = 237
const m__SC_V7_ILP32_OFFBIG = 238
const m__SC_V7_LP64_OFF64 = 239
const m__SC_V7_LPBIG_OFFBIG = 240
const m__SC_VERSION = 29
const m__SC_XBS5_ILP32_OFF32 = 125
const m__SC_XBS5_ILP32_OFFBIG = 126
const m__SC_XBS5_LP64_OFF64 = 127
const m__SC_XBS5_LPBIG_OFFBIG = 128
const m__SC_XOPEN_CRYPT = 92
const m__SC_XOPEN_ENH_I18N = 93
const m__SC_XOPEN_LEGACY = 129
const m__SC_XOPEN_REALTIME = 130
const m__SC_XOPEN_REALTIME_THREADS = 131
const m__SC_XOPEN_SHM = 94
const m__SC_XOPEN_STREAMS = 246
const m__SC_XOPEN_UNIX = 91
const m__SC_XOPEN_VERSION = 89
const m__SC_XOPEN_XCU_VERSION = 90
const m__SC_XOPEN_XPG2 = 98
const m__SC_XOPEN_XPG3 = 99
const m__SC_XOPEN_XPG4 = 100
const m__STDC_PREDEF_H = 1
const m__SYS_CDEFS_H = 1
const m__TANDEM_SOURCE = 1
const m__XOPEN_ENH_I18N = 1
const m__XOPEN_UNIX = 1
const m__XOPEN_VERSION = 700
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_HLE_ACQUIRE = 65536
const m___ATOMIC_HLE_RELEASE = 131072
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BIG_ENDIAN = 4321
const m___BYTE_ORDER = 1234
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DIG__ = 15
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_IS_IEC_60559__ = 2
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DEC128_EPSILON__ = 1e-33
const m___DEC128_MANT_DIG__ = 34
const m___DEC128_MAX_EXP__ = 6145
const m___DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const m___DEC128_MIN__ = 1e-6143
const m___DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const m___DEC32_EPSILON__ = 1e-6
const m___DEC32_MANT_DIG__ = 7
const m___DEC32_MAX_EXP__ = 97
const m___DEC32_MAX__ = 9.999999e96
const m___DEC32_MIN__ = 1e-95
const m___DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const m___DEC64_EPSILON__ = 1e-15
const m___DEC64_MANT_DIG__ = 16
const m___DEC64_MAX_EXP__ = 385
const m___DEC64_MAX__ = "9.999999999999999E384"
const m___DEC64_MIN__ = 1e-383
const m___DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const m___DECIMAL_BID_FORMAT__ = 1
const m___DECIMAL_DIG__ = 17
const m___DEC_EVAL_METHOD__ = 2
const m___ELF__ = 1
const m___EXTENSIONS__ = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLOAT_WORD_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___FLT128_DECIMAL_DIG__ = 36
const m___FLT128_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT128_DIG__ = 33
const m___FLT128_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT128_HAS_DENORM__ = 1
const m___FLT128_HAS_INFINITY__ = 1
const m___FLT128_HAS_QUIET_NAN__ = 1
const m___FLT128_IS_IEC_60559__ = 2
const m___FLT128_MANT_DIG__ = 113
const m___FLT128_MAX_10_EXP__ = 4932
const m___FLT128_MAX_EXP__ = 16384
const m___FLT128_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT128_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT128_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT16_DECIMAL_DIG__ = 5
const m___FLT16_DENORM_MIN__ = 5.96046447753906250000000000000000000e-8
const m___FLT16_DIG__ = 3
const m___FLT16_EPSILON__ = 9.76562500000000000000000000000000000e-4
const m___FLT16_HAS_DENORM__ = 1
const m___FLT16_HAS_INFINITY__ = 1
const m___FLT16_HAS_QUIET_NAN__ = 1
const m___FLT16_IS_IEC_60559__ = 2
const m___FLT16_MANT_DIG__ = 11
const m___FLT16_MAX_10_EXP__ = 4
const m___FLT16_MAX_EXP__ = 16
const m___FLT16_MAX__ = 6.55040000000000000000000000000000000e+4
const m___FLT16_MIN__ = 6.10351562500000000000000000000000000e-5
const m___FLT16_NORM_MAX__ = 6.55040000000000000000000000000000000e+4
const m___FLT32X_DECIMAL_DIG__ = 17
const m___FLT32X_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT32X_DIG__ = 15
const m___FLT32X_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT32X_HAS_DENORM__ = 1
const m___FLT32X_HAS_INFINITY__ = 1
const m___FLT32X_HAS_QUIET_NAN__ = 1
const m___FLT32X_IS_IEC_60559__ = 2
const m___FLT32X_MANT_DIG__ = 53
const m___FLT32X_MAX_10_EXP__ = 308
const m___FLT32X_MAX_EXP__ = 1024
const m___FLT32X_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32X_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT32X_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT32_DECIMAL_DIG__ = 9
const m___FLT32_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT32_DIG__ = 6
const m___FLT32_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT32_HAS_DENORM__ = 1
const m___FLT32_HAS_INFINITY__ = 1
const m___FLT32_HAS_QUIET_NAN__ = 1
const m___FLT32_IS_IEC_60559__ = 2
const m___FLT32_MANT_DIG__ = 24
const m___FLT32_MAX_10_EXP__ = 38
const m___FLT32_MAX_EXP__ = 128
const m___FLT32_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT32_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT32_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT64X_DECIMAL_DIG__ = 36
const m___FLT64X_DENORM_MIN__ = 6.47517511943802511092443895822764655e-4966
const m___FLT64X_DIG__ = 33
const m___FLT64X_EPSILON__ = 1.92592994438723585305597794258492732e-34
const m___FLT64X_HAS_DENORM__ = 1
const m___FLT64X_HAS_INFINITY__ = 1
const m___FLT64X_HAS_QUIET_NAN__ = 1
const m___FLT64X_IS_IEC_60559__ = 2
const m___FLT64X_MANT_DIG__ = 113
const m___FLT64X_MAX_10_EXP__ = 4932
const m___FLT64X_MAX_EXP__ = 16384
const m___FLT64X_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64X_MIN__ = 3.36210314311209350626267781732175260e-4932
const m___FLT64X_NORM_MAX__ = "1.18973149535723176508575932662800702e+4932"
const m___FLT64_DECIMAL_DIG__ = 17
const m___FLT64_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___FLT64_DIG__ = 15
const m___FLT64_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___FLT64_HAS_DENORM__ = 1
const m___FLT64_HAS_INFINITY__ = 1
const m___FLT64_HAS_QUIET_NAN__ = 1
const m___FLT64_IS_IEC_60559__ = 2
const m___FLT64_MANT_DIG__ = 53
const m___FLT64_MAX_10_EXP__ = 308
const m___FLT64_MAX_EXP__ = 1024
const m___FLT64_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT64_MIN__ = 2.22507385850720138309023271733240406e-308
const m___FLT64_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846432481707092372958328991613e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209289550781250000000000000000000e-7
const m___FLT_EVAL_METHOD_TS_18661_3__ = 0
const m___FLT_EVAL_METHOD__ = 0
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_IS_IEC_60559__ = 2
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_MIN__ = 1.17549435082228750796873653722224568e-38
const m___FLT_NORM_MAX__ = 3.40282346638528859811704183484516925e+38
const m___FLT_RADIX__ = 2
const m___FUNCTION__ = "__func__"
const m___FXSR__ = 1
const m___GCC_ASM_FLAG_OUTPUTS__ = 1
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_CONSTRUCTIVE_SIZE = 64
const m___GCC_DESTRUCTIVE_SIZE = 64
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GCC_IEC_559 = 2
const m___GCC_IEC_559_COMPLEX = 2
const m___GNUC_EXECUTION_CHARSET_NAME = "UTF-8"
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 0
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC_WIDE_EXECUTION_CHARSET_NAME = "UTF-32LE"
const m___GNUC__ = 12
const m___GXX_ABI_VERSION = 1017
const m___HAVE_GENERIC_SELECTION = 0
const m___HAVE_SPECULATION_SAFE_VALUE = 1
const m___INT16_MAX__ = 0x7fff
const m___INT32_MAX__ = 0x7fffffff
const m___INT32_TYPE__ = "int"
const m___INT64_MAX__ = 0x7fffffffffffffff
const m___INT8_MAX__ = 0x7f
const m___INTMAX_MAX__ = 0x7fffffffffffffff
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_MAX__ = 0x7fffffffffffffff
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_MAX__ = 0x7fffffffffffffff
const m___INT_FAST16_WIDTH__ = 64
const m___INT_FAST32_MAX__ = 0x7fffffffffffffff
const m___INT_FAST32_WIDTH__ = 64
const m___INT_FAST64_MAX__ = 0x7fffffffffffffff
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_MAX__ = 0x7f
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_MAX__ = 0x7fff
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_MAX__ = 0x7fffffff
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_MAX__ = 0x7fffffffffffffff
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_MAX__ = 0x7f
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 0x7fffffff
const m___INT_WIDTH__ = 32
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.94065645841246544176568792868221372e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.22044604925031308084726333618164062e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_IS_IEC_60559__ = 2
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LDBL_MIN__ = 2.22507385850720138309023271733240406e-308
const m___LDBL_NORM_MAX__ = 1.79769313486231570814527423731704357e+308
const m___LDOUBLE_REDIRECTS_TO_FLOAT128_ABI = 0
const m___LITTLE_ENDIAN = 1234
const m___LONG_DOUBLE_64__ = 1
const m___LONG_LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_LONG_WIDTH__ = 64
const m___LONG_MAX = 0x7fffffffffffffff
const m___LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___MMX_WITH_SSE__ = 1
const m___MMX__ = 1
const m___NO_INLINE__ = 1
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PDP_ENDIAN = 3412
const m___PIC__ = 2
const m___PIE__ = 2
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTRDIFF_MAX__ = 0x7fffffffffffffff
const m___PTRDIFF_WIDTH__ = 64
const m___REENTRANT = 1
const m___SCHAR_MAX__ = 0x7f
const m___SCHAR_WIDTH__ = 8
const m___SEG_FS = 1
const m___SEG_GS = 1
const m___SHRT_MAX__ = 0x7fff
const m___SHRT_WIDTH__ = 16
const m___SIG_ATOMIC_MAX__ = 0x7fffffff
const m___SIG_ATOMIC_TYPE__ = "int"
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT128__ = 16
const m___SIZEOF_FLOAT80__ = 16
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_MAX__ = 0xffffffffffffffff
const m___SIZE_WIDTH__ = 64
const m___SSE2_MATH__ = 1
const m___SSE2__ = 1
const m___SSE_MATH__ = 1
const m___SSE__ = 1
const m___STDC_HOSTED__ = 1
const m___STDC_IEC_559_COMPLEX__ = 1
const m___STDC_IEC_559__ = 1
const m___STDC_IEC_60559_BFP__ = 201404
const m___STDC_IEC_60559_COMPLEX__ = 201404
const m___STDC_ISO_10646__ = 201706
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC_WANT_IEC_60559_ATTRIBS_EXT__ = 1
const m___STDC_WANT_IEC_60559_BFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_DFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_FUNCS_EXT__ = 1
const m___STDC_WANT_IEC_60559_TYPES_EXT__ = 1
const m___STDC_WANT_LIB_EXT2__ = 1
const m___STDC_WANT_MATH_SPEC_FUNCS__ = 1
const m___STDC__ = 1
const m___SYSCALL_WORDSIZE = 64
const m___UINT16_MAX__ = 0xffff
const m___UINT32_MAX__ = 0xffffffff
const m___UINT64_MAX__ = 0xffffffffffffffff
const m___UINT8_MAX__ = 0xff
const m___UINTMAX_MAX__ = 0xffffffffffffffff
const m___UINTPTR_MAX__ = 0xffffffffffffffff
const m___UINT_FAST16_MAX__ = 0xffffffffffffffff
const m___UINT_FAST32_MAX__ = 0xffffffffffffffff
const m___UINT_FAST64_MAX__ = 0xffffffffffffffff
const m___UINT_FAST8_MAX__ = 0xff
const m___UINT_LEAST16_MAX__ = 0xffff
const m___UINT_LEAST32_MAX__ = 0xffffffff
const m___UINT_LEAST64_MAX__ = 0xffffffffffffffff
const m___UINT_LEAST8_MAX__ = 0xff
const m___USE_TIME_BITS64 = 1
const m___VERSION__ = "12.2.0"
const m___WCHAR_MAX__ = 0x7fffffff
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 0xffffffff
const m___WINT_MIN__ = 0
const m___WINT_WIDTH__ = 32
const m___WORDSIZE = 64
const m___WORDSIZE_TIME64_COMPAT32 = 1
const m___amd64 = 1
const m___amd64__ = 1
const m___code_model_small__ = 1
const m___glibc_c99_flexarr_available = 1
const m___gnu_linux__ = 1
const m___inline = "inline"
const m___k8 = 1
const m___k8__ = 1
const m___linux = 1
const m___linux__ = 1
const m___pic__ = 2
const m___pie__ = 2
const m___restrict = "restrict"
const m___ucontext = "ucontext"
const m___unix = 1
const m___unix__ = 1
const m___x86_64 = 1
const m___x86_64__ = 1
const m_alloca = "__builtin_alloca"
const m_linux = 1
const m_static_assert = "_Static_assert"
const m_unix = 1

type T__builtin_va_list = uintptr

type T__predefined_size_t = uint64

type T__predefined_wchar_t = int32

type T__predefined_ptrdiff_t = int64

type Tsize_t = uint64

type Ttime_t = int64

type Tclock_t = int64

type Ttimespec = struct {
	Ftv_sec  Ttime_t
	Ftv_nsec int64
}

type Tpid_t = int32

type Tuid_t = uint32

type Tpthread_t = uintptr

type Tsigset_t = struct {
	F__bits [16]uint64
}

type T__sigset_t = Tsigset_t

type Tpthread_attr_t = struct {
	F__u struct {
		F__vi [0][14]int32
		F__s  [0][7]uint64
		F__i  [14]int32
	}
}

type Tstack_t = struct {
	Fss_sp    uintptr
	Fss_flags int32
	Fss_size  Tsize_t
}

type Tsigaltstack = Tstack_t

const _REG_R8 = 0
const _REG_R9 = 1
const _REG_R10 = 2
const _REG_R11 = 3
const _REG_R12 = 4
const _REG_R13 = 5
const _REG_R14 = 6
const _REG_R15 = 7
const _REG_RDI = 8
const _REG_RSI = 9
const _REG_RBP = 10
const _REG_RBX = 11
const _REG_RDX = 12
const _REG_RAX = 13
const _REG_RCX = 14
const _REG_RSP = 15
const _REG_RIP = 16
const _REG_EFL = 17
const _REG_CSGSFS = 18
const _REG_ERR = 19
const _REG_TRAPNO = 20
const _REG_OLDMASK = 21
const _REG_CR2 = 22

type Tgreg_t = int64

type Tgregset_t = [23]int64

type Tfpregset_t = uintptr

type T_fpstate = struct {
	Fcwd       uint16
	Fswd       uint16
	Fftw       uint16
	Ffop       uint16
	Frip       uint64
	Frdp       uint64
	Fmxcsr     uint32
	Fmxcr_mask uint32
	F_st       [8]struct {
		Fsignificand [4]uint16
		Fexponent    uint16
		Fpadding     [3]uint16
	}
	F_xmm [16]struct {
		Felement [4]uint32
	}
	Fpadding [24]uint32
}

type Tsigcontext = struct {
	Fr8          uint64
	Fr9          uint64
	Fr10         uint64
	Fr11         uint64
	Fr12         uint64
	Fr13         uint64
	Fr14         uint64
	Fr15         uint64
	Frdi         uint64
	Frsi         uint64
	Frbp         uint64
	Frbx         uint64
	Frdx         uint64
	Frax         uint64
	Frcx         uint64
	Frsp         uint64
	Frip         uint64
	Feflags      uint64
	Fcs          uint16
	Fgs          uint16
	Ffs          uint16
	F__pad0      uint16
	Ferr         uint64
	Ftrapno      uint64
	Foldmask     uint64
	Fcr2         uint64
	Ffpstate     uintptr
	F__reserved1 [8]uint64
}

type Tmcontext_t = struct {
	Fgregs       Tgregset_t
	Ffpregs      Tfpregset_t
	F__reserved1 [8]uint64
}

type Tucontext_t = struct {
	Fuc_flags     uint64
	Fuc_link      uintptr
	Fuc_stack     Tstack_t
	Fuc_mcontext  Tmcontext_t
	Fuc_sigmask   Tsigset_t
	F__fpregs_mem [64]uint64
}

type Tucontext = Tucontext_t

type Tsigval = struct {
	Fsival_ptr   [0]uintptr
	Fsival_int   int32
	F__ccgo_pad2 [4]byte
}

type Tsiginfo_t = struct {
	Fsi_signo    int32
	Fsi_errno    int32
	Fsi_code     int32
	F__si_fields struct {
		F__si_common [0]struct {
			F__first struct {
				F__timer [0]struct {
					Fsi_timerid int32
					Fsi_overrun int32
				}
				F__piduid struct {
					Fsi_pid Tpid_t
					Fsi_uid Tuid_t
				}
			}
			F__second struct {
				F__sigchld [0]struct {
					Fsi_status int32
					Fsi_utime  Tclock_t
					Fsi_stime  Tclock_t
				}
				Fsi_value    Tsigval
				F__ccgo_pad2 [16]byte
			}
		}
		F__sigfault [0]struct {
			Fsi_addr     uintptr
			Fsi_addr_lsb int16
			F__first     struct {
				Fsi_pkey    [0]uint32
				F__addr_bnd struct {
					Fsi_lower uintptr
					Fsi_upper uintptr
				}
			}
		}
		F__sigpoll [0]struct {
			Fsi_band int64
			Fsi_fd   int32
		}
		F__sigsys [0]struct {
			Fsi_call_addr uintptr
			Fsi_syscall   int32
			Fsi_arch      uint32
		}
		F__pad [112]int8
	}
}

type Tsigaction = struct {
	F__sa_handler struct {
		Fsa_sigaction [0]uintptr
		Fsa_handler   uintptr
	}
	Fsa_mask     Tsigset_t
	Fsa_flags    int32
	Fsa_restorer uintptr
}

type Tsigevent = struct {
	Fsigev_value  Tsigval
	Fsigev_signo  int32
	Fsigev_notify int32
	F__sev_fields struct {
		Fsigev_notify_thread_id [0]Tpid_t
		F__sev_thread           [0]struct {
			Fsigev_notify_function   uintptr
			Fsigev_notify_attributes uintptr
		}
		F__pad [48]int8
	}
}

type Tsig_t = uintptr

type Tsighandler_t = uintptr

type Tsig_atomic_t = int32

type Twchar_t = int32

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tssize_t = int64

type Tregister_t = int64

type Tsuseconds_t = int64

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tu_int64_t = uint64

type Tmode_t = uint32

type Tnlink_t = uint64

type Toff_t = int64

type Tino_t = uint64

type Tdev_t = uint64

type Tblksize_t = int64

type Tblkcnt_t = int64

type Tfsblkcnt_t = uint64

type Tfsfilcnt_t = uint64

type Ttimer_t = uintptr

type Tclockid_t = int32

type Tid_t = uint32

type Tgid_t = uint32

type Tkey_t = int32

type Tuseconds_t = uint32

type Tpthread_once_t = int32

type Tpthread_key_t = uint32

type Tpthread_spinlock_t = int32

type Tpthread_mutexattr_t = struct {
	F__attr uint32
}

type Tpthread_condattr_t = struct {
	F__attr uint32
}

type Tpthread_barrierattr_t = struct {
	F__attr uint32
}

type Tpthread_rwlockattr_t = struct {
	F__attr [2]uint32
}

type Tpthread_mutex_t = struct {
	F__u struct {
		F__vi [0][10]int32
		F__p  [0][5]uintptr
		F__i  [10]int32
	}
}

type Tpthread_cond_t = struct {
	F__u struct {
		F__vi [0][12]int32
		F__p  [0][6]uintptr
		F__i  [12]int32
	}
}

type Tpthread_rwlock_t = struct {
	F__u struct {
		F__vi [0][14]int32
		F__p  [0][7]uintptr
		F__i  [14]int32
	}
}

type Tpthread_barrier_t = struct {
	F__u struct {
		F__vi [0][8]int32
		F__p  [0][4]uintptr
		F__i  [8]int32
	}
}

type Tu_int8_t = uint8

type Tu_int16_t = uint16

type Tu_int32_t = uint32

type Tcaddr_t = uintptr

type Tu_char = uint8

type Tu_short = uint16

type Tushort = uint16

type Tu_int = uint32

type Tuint = uint32

type Tu_long = uint64

type Tulong = uint64

type Tquad_t = int64

type Tu_quad_t = uint64

type Tuint16_t = uint16

type Tuint32_t = uint32

type Tuint64_t = uint64

type Ttimeval = struct {
	Ftv_sec  Ttime_t
	Ftv_usec Tsuseconds_t
}

type Tfd_mask = uint64

type Tfd_set = struct {
	Ffds_bits [16]uint64
}

type Tuintptr_t = uint64

type Tintptr_t = int64

type Tintmax_t = int64

type Tuint8_t = uint8

type Tuintmax_t = uint64

type Tint_fast8_t = int8

type Tint_fast64_t = int64

type Tint_least8_t = int8

type Tint_least16_t = int16

type Tint_least32_t = int32

type Tint_least64_t = int64

type Tuint_fast8_t = uint8

type Tuint_fast64_t = uint64

type Tuint_least8_t = uint8

type Tuint_least16_t = uint16

type Tuint_least32_t = uint32

type Tuint_least64_t = uint64

type Tint_fast16_t = int32

type Tint_fast32_t = int32

type Tuint_fast16_t = uint32

type Tuint_fast32_t = uint32

type Tva_list = uintptr

type T__isoc_va_list = uintptr

type Tfpos_t = struct {
	F__lldata [0]int64
	F__align  [0]float64
	F__opaque [16]int8
}

type T_G_fpos64_t = Tfpos_t

type Tcookie_io_functions_t = struct {
	Fread   uintptr
	Fwrite  uintptr
	Fseek   uintptr
	Fclose1 uintptr
}

type T_IO_cookie_io_functions_t = Tcookie_io_functions_t

type Tstat = struct {
	Fst_dev     Tdev_t
	Fst_ino     Tino_t
	Fst_nlink   Tnlink_t
	Fst_mode    Tmode_t
	Fst_uid     Tuid_t
	Fst_gid     Tgid_t
	F__pad0     uint32
	Fst_rdev    Tdev_t
	Fst_size    Toff_t
	Fst_blksize Tblksize_t
	Fst_blocks  Tblkcnt_t
	Fst_atim    Ttimespec
	Fst_mtim    Ttimespec
	Fst_ctim    Ttimespec
	F__unused   [3]int64
}

type Tstatx_timestamp = struct {
	Ftv_sec  Tint64_t
	Ftv_nsec Tuint32_t
	F__pad   Tuint32_t
}

type Tstatx = struct {
	Fstx_mask            Tuint32_t
	Fstx_blksize         Tuint32_t
	Fstx_attributes      Tuint64_t
	Fstx_nlink           Tuint32_t
	Fstx_uid             Tuint32_t
	Fstx_gid             Tuint32_t
	Fstx_mode            Tuint16_t
	F__pad0              [1]Tuint16_t
	Fstx_ino             Tuint64_t
	Fstx_size            Tuint64_t
	Fstx_blocks          Tuint64_t
	Fstx_attributes_mask Tuint64_t
	Fstx_atime           Tstatx_timestamp
	Fstx_btime           Tstatx_timestamp
	Fstx_ctime           Tstatx_timestamp
	Fstx_mtime           Tstatx_timestamp
	Fstx_rdev_major      Tuint32_t
	Fstx_rdev_minor      Tuint32_t
	Fstx_dev_major       Tuint32_t
	Fstx_dev_minor       Tuint32_t
	F__pad1              [14]Tuint64_t
}

type Tlocale_t = uintptr

// C documentation
//
//	/* 128 bits of random data. */
var _secret = [16]int8{
	0:  libc.Int8FromInt32(0xa0),
	1:  int8(0x6c),
	2:  int8(0x0c),
	3:  libc.Int8FromInt32(0x81),
	4:  libc.Int8FromInt32(0xba),
	5:  libc.Int8FromInt32(0xd8),
	6:  int8(0x5b),
	7:  int8(0x0c),
	8:  libc.Int8FromInt32(0xb0),
	9:  libc.Int8FromInt32(0xd6),
	10: libc.Int8FromInt32(0xd4),
	11: libc.Int8FromInt32(0xe3),
	12: libc.Int8FromInt32(0xeb),
	13: int8(0x52),
	14: int8(0x5f),
	15: libc.Int8FromInt32(0x96),
}

const _SECRETCOUNT = 64
const _SECRETBYTES = 1024

// C documentation
//
//	/*
//	 * As of glibc 2.34, when _GNU_SOURCE is defined, SIGSTKSZ is no longer
//	 * constant on Linux. SIGSTKSZ is redefined to sysconf (_SC_SIGSTKSZ).
//	 */
var _altstack uintptr

func _setup_stack(tls *libc.TLS) {
	var sigstk Tstack_t
	var v1 uintptr
	_, _ = sigstk, v1
	v1 = libc.Xcalloc(tls, uint64(1), libc.Uint64FromInt32(libc.Int32FromInt32(m_SIGSTKSZ)+int32(_SECRETBYTES)))
	_altstack = v1
	sigstk = Tstack_t{
		Fss_sp:   v1,
		Fss_size: libc.Uint64FromInt32(libc.Int32FromInt32(m_SIGSTKSZ) + int32(_SECRETBYTES)),
	}
}

func _cleanup_stack(tls *libc.TLS) {
	libc.Xfree(tls, _altstack)
}

func _assert_on_stack(tls *libc.TLS) {
	var cursigstk Tstack_t
	_ = cursigstk
}

func _call_on_stack(tls *libc.TLS, fn uintptr) {
	var oldsigact, sigact Tsigaction
	var oldsigset, sigset Tsigset_t
	_, _, _, _ = oldsigact, oldsigset, sigact, sigset
	/*
	 * This is a bit more complicated than strictly necessary, but
	 * it ensures we don't have any flaky test failures due to
	 * inherited signal masks/actions/etc.
	 *
	 * On systems where SA_ONSTACK is not supported, this could
	 * alternatively be implemented using makecontext() or
	 * pthread_attr_setstack().
	 */
	*(*uintptr)(unsafe.Add(unsafe.Pointer(&sigact), 0)) = fn
	*(*int32)(unsafe.Add(unsafe.Pointer(&sigact), 136)) = int32(m_SA_ONSTACK)
	/* First, block all signals. */
	/* Next setup the signal handler for SIGUSR1. */
	/* Raise SIGUSR1 and momentarily unblock it to run the handler. */
	/* Restore the original signal action, stack, and mask. */
}

func _populate_secret(tls *libc.TLS, buf uintptr, len1 Tssize_t) {
	var fds [2]int32
	var i int32
	_, _ = fds, i
	i = 0
	for {
		if !(i < int32(_SECRETCOUNT)) {
			break
		}
		goto _1
	_1:
		;
		i++
	}
}

func _blank_stack_side_effects(tls *libc.TLS, buf uintptr, len1 Tsize_t) {
	bp := tls.Alloc(4096)
	defer tls.Free(4096)
	var _ /* scratch at bp+0 */ [4096]int8
	/* If the read(3) in populate_secret() wrote into the stack, as it
	 * might happen on the Hurd for small data, then we might incorrectly
	 * detect the wrong secret on the stack. */
	libc.Xmemset(tls, bp, int32(0xFF), uint64(4096))
}

func _count_secrets(tls *libc.TLS, buf uintptr) (r int32) {
	var i Tsize_t
	var res int32
	_, _ = i, res
	res = 0
	i = uint64(0)
	for {
		if !(i < uint64(_SECRETCOUNT)) {
			break
		}
		if libc.Xmemcmp(tls, buf+uintptr(i*uint64(16)), uintptr(unsafe.Pointer(&_secret)), uint64(16)) == 0 {
			res += int32(1)
		}
		goto _1
	_1:
		;
		i++
	}
	return res
}

func _test_without_bzero(tls *libc.TLS) (r uintptr) {
	bp := tls.Alloc(1024)
	defer tls.Free(1024)
	var res uintptr
	var _ /* buf at bp+0 */ [1024]int8
	_ = res
	_assert_on_stack(tls)
	_populate_secret(tls, bp, int64(1024))
	_blank_stack_side_effects(tls, bp, uint64(1024))
	res = libc.Xmemmem(tls, _altstack, libc.Uint64FromInt32(libc.Int32FromInt32(m_SIGSTKSZ)+int32(_SECRETBYTES)), bp, uint64(1024))
	return res
}

func _test_with_bzero(tls *libc.TLS) (r uintptr) {
	bp := tls.Alloc(1024)
	defer tls.Free(1024)
	var res uintptr
	var _ /* buf at bp+0 */ [1024]int8
	_ = res
	_assert_on_stack(tls)
	_populate_secret(tls, bp, int64(1024))
	_blank_stack_side_effects(tls, bp, uint64(1024))
	res = libc.Xmemmem(tls, _altstack, libc.Uint64FromInt32(libc.Int32FromInt32(m_SIGSTKSZ)+int32(_SECRETBYTES)), bp, uint64(1024))
	libbsd.Xexplicit_bzero(tls, bp, uint64(1024))
	return res
}

func _do_test_without_bzero(tls *libc.TLS, signo int32) {
	var buf uintptr
	_ = buf
	buf = _test_without_bzero(tls)
}

func _do_test_with_bzero(tls *libc.TLS, signo int32) {
	var buf uintptr
	_ = buf
	buf = _test_with_bzero(tls)
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	_setup_stack(tls)
	/*
	 * Solaris and OS X clobber the signal stack after returning to the
	 * normal stack, so we need to inspect altstack while we're still
	 * running on it.  Unfortunately, this means we risk clobbering the
	 * buffer ourselves.
	 *
	 * To minimize this risk, test_with{,out}_bzero() are responsible for
	 * locating the offset of their buf variable within altstack, and
	 * and returning that address.  Then we can simply memcmp() repeatedly
	 * to count how many instances of secret we found.
	 */
	/*
	 * First, test that if we *don't* call explicit_bzero, that we
	 * *are* able to find at least one instance of the secret data still
	 * on the stack.  This sanity checks that call_on_stack() and
	 * populate_secret() work as intended.
	 */
	libc.Xmemset(tls, _altstack, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SIGSTKSZ)+int32(_SECRETBYTES)))
	_call_on_stack(tls, __ccgo_fp(_do_test_without_bzero))
	/*
	 * Now test with a call to explicit_bzero() and check that we
	 * *don't* find any instances of the secret data.
	 */
	libc.Xmemset(tls, _altstack, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SIGSTKSZ)+int32(_SECRETBYTES)))
	_call_on_stack(tls, __ccgo_fp(_do_test_with_bzero))
	_cleanup_stack(tls)
	return 0
}

func main() {
	libc.Start(x_main)
}

func __ccgo_fp(f interface{}) uintptr {
	type iface [2]uintptr
	return (*iface)(unsafe.Pointer(&f))[1]
}
