// Copyright 2023 The libbsd-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libbsd // import "modernc.org/libbsd"

import (
	"context"
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"testing"
	"time"

	_ "modernc.org/cc/v4"
	_ "modernc.org/ccgo/v4/lib"
	_ "modernc.org/fileutil/ccgo"
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
	target = fmt.Sprintf("%s/%s", goos, goarch)
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

var blacklist = map[string]map[string]struct{}{
	"linux/386": {
		"closefrom":      {},
		"fgetln":         {},
		"fparseln":       {},
		"funopen":        {},
		"nlist":          {},
		"proctitle":      {},
		"proctitle-init": {},
		"progname":       {},
		"setmode":        {},
		"strmode":        {},
		"strtonum":       {},
		"vis":            {},
		"vis-openbsd":    {},
	},
	"linux/arm": {
		"closefrom":      {},
		"fgetln":         {},
		"fparseln":       {},
		"funopen":        {},
		"nlist":          {},
		"proctitle":      {},
		"proctitle-init": {},
		"progname":       {},
		"setmode":        {},
		"strmode":        {},
		"strtonum":       {},
		"vis":            {},
		"vis-openbsd":    {},
	},
	"linux/riscv64": {
		"closefrom":      {},
		"fgetln":         {},
		"fparseln":       {},
		"funopen":        {},
		"nlist":          {},
		"proctitle":      {},
		"proctitle-init": {},
		"progname":       {},
		"setmode":        {},
		"strmode":        {},
		"strtonum":       {},
		"timeconv64":     {},
		"vis":            {},
		"vis-openbsd":    {},
	},
	"linux/s390x": {
		"closefrom":      {},
		"explicit_bzero": {},
		"fgetln":         {},
		"fparseln":       {},
		"funopen":        {},
		"nlist":          {},
		"proctitle":      {},
		"proctitle-init": {},
		"progname":       {},
		"setmode":        {},
		"strmode":        {},
		"strtonum":       {},
		"vis":            {},
		"vis-openbsd":    {},
	},
	"linux/amd64": {
		"closefrom":      {},
		"fgetln":         {},
		"fparseln":       {},
		"fpurge":         {},
		"funopen":        {},
		"nlist":          {},
		"proctitle":      {},
		"proctitle-init": {},
		"progname":       {},
		"setmode":        {},
		"strmode":        {},
		"strtonum":       {},
		"timeconv64":     {},
		"vis":            {},
		"vis-openbsd":    {},
	},
	"linux/arm64": {
		"closefrom": {},
		"nlist":     {},
		"progname":  {},
		"setmode":   {},
		"strmode":   {},
		"strtonum":  {},
	},
	"linux/ppc64le": {
		"closefrom":      {},
		"fgetln":         {},
		"fparseln":       {},
		"funopen":        {},
		"nlist":          {},
		"proctitle":      {},
		"proctitle-init": {},
		"progname":       {},
		"setmode":        {},
		"strmode":        {},
		"strtonum":       {},
		"timeconv64":     {},
		"vis":            {},
		"vis-openbsd":    {},
	},
	"linux/loong64": {
		"closefrom":      {},
		"fgetln":         {},
		"fparseln":       {},
		"funopen":        {},
		"nlist":          {},
		"proctitle":      {},
		"proctitle-init": {},
		"progname":       {},
		"setmode":        {},
		"strmode":        {},
		"strtonum":       {},
		"timeconv64":     {},
		"vis":            {},
		"vis-openbsd":    {},
	},
	"darwin/amd64": {
		"bzero":          {},
		"closefrom":      {},
		"endian":         {},
		"explicit_bzero": {},
		"fpurge":         {},
		"md5":            {},
		"nlist":          {},
		"proctitle-init": {},
		"progname":       {},
		"setmode":        {},
		"strl":           {},
		"strmode":        {},
		"strnstr":        {},
		"strtonum":       {},
		"timeconv64":     {},
	},
	"darwin/arm64": {
		"bzero":          {},
		"closefrom":      {},
		"endian":         {},
		"explicit_bzero": {},
		"fpurge":         {},
		"md5":            {},
		"nlist":          {},
		"proctitle-init": {},
		"progname":       {},
		"setmode":        {},
		"strl":           {},
		"strmode":        {},
		"strnstr":        {},
		"strtonum":       {},
		"timeconv64":     {},
	},
}

func Test(t *testing.T) {
	switch target {
	case "freebsd/amd64":
		t.Skip("TODO")
	}

	tempDir := t.TempDir()
	var files, skip, buildok, fail, pass int
	root := filepath.Join("internal", "tests")
	filepath.Walk(filepath.Join("internal", "tests"), func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			t.Fatal(err)
		}

		if path == root || !info.IsDir() {
			return nil
		}

		files++
		if _, ok := blacklist[target][filepath.Base(path)]; ok {
			skip++
			return nil
		}

		bin := filepath.Join(tempDir, "test")
		os.Remove(bin)

		out, err := run(5*time.Minute, "", "go", "build", "-o", bin, "./"+path)
		if err != nil {
			t.Errorf("%s: FAIL err=%v out=%s", path, err, out)
			return nil
		}

		buildok++
		if out, err = run(5*time.Minute, "", bin, "-v"); err != nil {
			fail++
			t.Errorf("%s: FAIL err=%v out=%s", path, err, out)
			return nil
		}

		t.Logf("%s: PASS", path)
		pass++
		return nil
	})

	t.Logf("files=%v skip=%v buildok=%v fail=%v pass=%v", files, skip, buildok, fail, pass)
	// 202403141721: all_test.go:64: files=25 skip=0 buildok=10 fail=1 pass=9
	// 202403181830: all_test.go:92: files=10 skip=0 buildok=10 fail=0 pass=10
}

func run(limit time.Duration, inDir, bin string, args ...string) (out []byte, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), limit)

	defer cancel()

	cmd := exec.CommandContext(ctx, bin, args...)
	cmd.Dir = inDir
	cmd.WaitDelay = 10 * time.Second
	return cmd.CombinedOutput()
}
